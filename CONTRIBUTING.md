# Contributing to Flashbulb

We love patches of all kinds -- code, comments, UI, documentation, and more.
Please consider submitting a patch instead of making a competing project, even
if your patch diverges wildly from what the project is today.

By participating in this project to agree to abide by our [code of conduct].

## Governance

Your interaction with this project can be divided into three sections: you as a
contributor, you as a committer, and how to become a committer.

### Commenter and meta-contributor

Those commenting and triaging issues and merge requests are expected to adhere
to our [code of conduct].

### Contributor

This project is currently under rapid development. The maintainers' short-term
goal is to get up to the features required for public beta testing. Expect
formal releases to be haphazard during this time.

As a contributor, you can expect a maintainer to add a label to your merge
request or issue within two weeks. This indicates that a maintainer has seen
your contribution and quickly triaged it. If a maintainer believe that your
feature request will not be merged, they will tell you as much during the
triage step.

Bug reports, either as issues or as merge requests, get the maintainers'
priority. The maintainers consider documentation bugs to be as important as
code bugs.

Feature contributions (that is, a merge request that add a new feature) get
lower priority. A maintainer will evaluate the patch carefully for
maintainability.

During this rapid development period, feature requests without a patch will be
closed. A maintainer will try to provide a description of how to write your
patch while we close the request, though they cannot guarantee this.

Interactions outside of GitLib Issues and Merge Requests are not considered
contributing. For example, tooting about a bug in Flashbulb is not contributing
to Flashbulb, and therefore we can make no guarantees or promises about such an
action.

You are expected to adhere to our [code of conduct].

Committer
---------

As a committer, you can merge at any time. The maintainers encourage you to
have the code reviewed by someone beforehand.

You are expected to adhere to our [code of conduct].

Maintainer
----------

A maintainer acts as a project manager and, as such, has final say and
responsibility.

All maintainers must adhere to our [code of conduct].

Promotion
---------

Contributors with two merged patches are welcome to request committer access by
opening a private direct message with [@mikeburns@mastodon.technology].

Existing contributors become a maintainer by taking on more work and
responsibility.

[code of conduct]: CODE_OF_CONDUCT.md
[@mikeburns@mastodon.technology]: https://mastodon.technology/@mikeburns
