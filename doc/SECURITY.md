# Security

Please open a [confidential issue] if you are reporting a security concern.

[confidential issue]: https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html
