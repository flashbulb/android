# Flashbulb

Flashbulb is an Android Mastodon and PixelFed client with a focus on media.
It's an app that works with any Mastodon-compatible API. The UX centers on and
is designed around media posts.

## Installation

See [the Web site](https://flashbulb.app/) for installing the app.

All versions of the APK are available from [the GitLab tags].

## Additional documentation

* [CODE_OF_CONDUCT.md](doc/CODE_OF_CONDUCT.md)
* [CONTRIBUTING.md](CONTRIBUTING.md)
* [LICENSE](LICENSE)
* [NEWS](NEWS)
* [RELEASING.md](doc/RELEASING.md)
* [SECURITY.md](doc/SECURITY.md)

## Author

Copyright 2018 Mike Burns. Licensed under [GNU GPL 3].

The icons [camera icon], [vertical dots icon], [heart], [heart pulse], [heart
outline], [sync], [sync-alert], [sync-off], [earth], [email], [lock],
[lock-open], [account multiple], [refresh], and [palette] are by Google,
licensed under the [Apache license 2.0].

The icons [light bulb icon], [heart broken], and [reload] are by [Austin
Andrews], licensed under the [SIL Open Font License, Version 1.1].

The icon [cloud question] is by [Michael Irigoyen], licensed under the [SIL
Open Font License, Version 1.1].

The icon [image off] by [Green Turtwig], licensed under the [SIL Open Font License,
Version 1.1].

[Code of conduct] via [Contributor Covenant], released under the [Creative
Commons Attribution 4.0 International Public License].

[the GitLab tags]: https://gitlab.com/flashbulb/android/tags
[GNU GPL 3]: [LICENSE]
[Austin Andrews]: https://twitter.com/Templarian
[Green Turtwig]: http://twitter.com/greenturtwig
[Michael Irigoyen]: http://twitter.com/mririgo
[SIL Open Font License, Version 1.1]: http://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web
[Creative Commons Attribution 4.0 International Public License]: https://github.com/ContributorCovenant/contributor_covenant/blob/master/LICENSE.md
[Apache license 2.0]: https://www.apache.org/licenses/LICENSE-2.0
[Code of conduct]: doc/CODE_OF_CONDUCT.md
[Contributor Covenant]: https://www.contributor-covenant.org/
[camera icon]: https://materialdesignicons.com/icon/camera
[vertical dots icon]: https://materialdesignicons.com/icon/dots-vertical
[light bulb icon]: https://materialdesignicons.com/icon/lightbulb-on-outline
[image off]: https://materialdesignicons.com/icon/image-off
[heart]: https://materialdesignicons.com/icon/heart
[heart pulse]: https://materialdesignicons.com/icon/heart-pulse
[heart outline]: https://materialdesignicons.com/icon/heart-outline
[heart broken]: https://materialdesignicons.com/icon/heart-broken
[reload]: https://materialdesignicons.com/icon/reload
[sync]: https://materialdesignicons.com/icon/sync
[sync-alert]: https://materialdesignicons.com/icon/sync-alert
[sync-off]: https://materialdesignicons.com/icon/sync-off
[earth]: https://materialdesignicons.com/icon/earth
[email]: https://materialdesignicons.com/icon/email
[lock]: https://materialdesignicons.com/icon/lock
[lock-open]: https://materialdesignicons.com/icon/lock-open
[cloud question]: https://materialdesignicons.com/icon/cloud-question
[account multiple]: https://materialdesignicons.com/icon/account-multiple
[refresh]: https://materialdesignicons.com/icon/refresh
[palette]: https://materialdesignicons.com/icon/palette
