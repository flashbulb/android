package app.flashbulb.android.feed

import androidx.fragment.app.FragmentManager
import app.flashbulb.android.api.PreviewableMedium

class MediaPagerAdapter(
        fragmentManager: FragmentManager,
        private val mediaAttachments: List<PreviewableMedium>,
        private val sensitive: Boolean
) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {
    private val fragments: Array<MediaFragment> by lazy {
        mediaAttachments.mapIndexed { offset, attachment ->
            MediaFragment().also { fragment ->
                fragment.arguments = MediaItem(
                        attachment.url,
                        attachment.previewUrl,
                        attachment.description,
                        attachment.type,
                        sensitive && offset == 0
                ).toBundle()
            }
        }.toTypedArray()
    }

    override fun getCount() = mediaAttachments.size

    override fun getItem(position: Int) = fragments[position]

    fun pause() {
        fragments.forEach { it.pause() }
    }
}
