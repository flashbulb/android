package app.flashbulb.android.feed

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation.findNavController
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.auth.AuthActivity
import app.flashbulb.android.post.NewStatusAlbumFragment
import app.flashbulb.android.support.ApiProvider
import app.flashbulb.android.support.FlashbulbActivity
import app.flashbulb.android.support.OnFragmentAttachedListener
import either.Either
import either.Left
import either.Right
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.IOException


class MainActivity : FlashbulbActivity(), OnFragmentAttachedListener, ApiProvider {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_feed)
    }

    override fun themeMapping(themeName: String) =
            when (themeName) {
                getString(R.string.light_theme_value) -> R.style.LightTheme_NoActionBar
                else -> R.style.DarkTheme_NoActionBar
            }

    override fun onFragmentAttached(fragment: androidx.fragment.app.Fragment) {
        (fragment as? NewStatusAlbumFragment)?.createImageFile = ::createImageFile
    }

    override fun withApi(callback: (Either<Int, Api>) -> Unit) {
        lifecycleScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                callback(currentAccount().api())
            }
        }
    }

    /**
     * An item from the appbar menu was selected.
     */
    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                R.id.manage_accounts -> {
                    showManageAccounts()
                    true
                }
                else -> {
                    super.onOptionsItemSelected(item)
                }
            }

    @SuppressLint("ApplySharedPref") // in coroutine so we can control the timing
    private fun showManageAccounts() {
        val accountNames = accounts().map { it.name }.toTypedArray()
        AlertDialog.Builder(this)
                .setTitle(R.string.manage_accounts)
                .setItems(accountNames) { _, which ->
                    lifecycleScope.launch(Dispatchers.Main) {
                        withContext(Dispatchers.Default) {
                            oauthPrefs.edit().putString(AuthActivity.ACCOUNT_NAME, accountNames[which]).commit()
                        }
                        startActivity(Intent(applicationContext, MainActivity::class.java))
                        finish()
                    }
                }
                .setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.cancel() }
                .setNeutralButton(R.string.add_account) { _, _ ->
                    startActivity(Intent(applicationContext, AuthActivity::class.java))
                }
                .show()
    }

    private fun createImageFile(): Either<Int, File> =
            try {
                val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                Right(File.createTempFile(packageName, ".jpg", storageDir))
            } catch (e: IOException) {
                Left(R.string.file_io_exception)
            }

    override fun onSupportNavigateUp() = findNavController(this, R.id.main_nav_host_fragment).navigateUp()
}
