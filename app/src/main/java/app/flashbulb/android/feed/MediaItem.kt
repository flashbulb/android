package app.flashbulb.android.feed

import android.os.Bundle
import app.flashbulb.android.api.MediaType

data class MediaItem(
        val url: String,
        val previewUrl: String?,
        val description: String?,
        val type: MediaType,
        val isSensitive: Boolean
) {
    companion object {
        private const val URL = "url"
        private const val PREVIEW_URL = "preview_url"
        private const val DESCRIPTION = "description"
        private const val SENSITIVE = "sensitive"
        private const val TYPE = "type"

        fun fromBundle(bundle: Bundle) = MediaItem(
                bundle.getString(URL)!!,
                bundle.getString(PREVIEW_URL)!!,
                bundle.getString(DESCRIPTION),
                MediaType.valueOf(bundle.getString(TYPE, MediaType.UNKNOWN.name)),
                bundle.getBoolean(SENSITIVE, true)
        )
    }

    fun toBundle() = Bundle().apply {
        putString(URL, url)
        putString(PREVIEW_URL, url)
        putString(DESCRIPTION, description)
        putString(TYPE, type.name)
        putBoolean(SENSITIVE, isSensitive)
    }
}
