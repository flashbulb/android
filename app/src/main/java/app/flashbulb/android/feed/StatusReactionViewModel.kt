package app.flashbulb.android.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.work.*
import kotlin.reflect.KClass

class StatusReactionViewModel(private val workManager: WorkManager, private val baseUrl: String, private val accessToken: String) : ViewModel() {
    fun fav(statusId: String): LiveData<WorkInfo> =
            reaction(statusId, FavStatusWorker::class)

    fun unfav(statusId: String): LiveData<WorkInfo> =
            reaction(statusId, UnfavStatusWorker::class)

    fun boost(statusId: String): LiveData<WorkInfo> =
            reaction(statusId, BoostStatusWorker::class)

    fun unboost(statusId: String): LiveData<WorkInfo> =
            reaction(statusId, UnboostStatusWorker::class)

    private fun reaction(statusId: String, workerClass: KClass<out Worker>): LiveData<WorkInfo> {
        val data = StatusReactionWorker.buildData(statusId, baseUrl, accessToken)
        val statusRequest = OneTimeWorkRequest.Builder(workerClass.java)
                .setInputData(data)
                .build()

        workManager.enqueue(statusRequest)

        return workManager.getWorkInfoByIdLiveData(statusRequest.id)
    }
}
