package app.flashbulb.android.feed

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.flashbulb.android.R
import app.flashbulb.android.databinding.ReportStatusBinding
import app.flashbulb.android.support.FlashbulbFragment
import app.flashbulb.android.support.OnFragmentAttachedListener
import app.flashbulb.android.support.errorString
import app.flashbulb.android.support.hideKeyboard
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class ReportStatusFragment : FlashbulbFragment() {
  override lateinit var rootLayout: View
  private var _binding: ReportStatusBinding? = null
  private val binding get() = _binding!!

  override fun onAttach(context: Context?) {
    super.onAttach(context)

    (context as? OnFragmentAttachedListener)?.onFragmentAttached(this)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View =
    ReportStatusBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    rootLayout = binding.reportStatusLayout

    binding.sendReportStatus.setOnClickListener(::sendReport)
  }

  private fun sendReport(view: View) {
    hideKeyboard(activity, view)
    binding.reportProgress.visibility = View.VISIBLE
    binding.sendReportStatus.visibility = View.GONE

    val commentText = binding.reportStatusComment.text.toString()
    val accountId = arguments?.getString(getString(R.string.account_id))
    val statusIds = arguments?.getStringArrayList(getString(R.string.status_ids))

    try {
      if (accountId != null && statusIds != null) {
        withApi { api ->
          lifecycleScope.launch(Dispatchers.Main) {
            try {
              val result = withContext(Dispatchers.Default) {
                api.reportAccount(accountId, statusIds, commentText).execute()
              }

              if (result.isSuccessful) {
                findNavController().popBackStack()

                snackbar(R.string.status_report_sent).show()
              } else {
                snackbar(
                  getString(R.string.status_report_failed_reason, result.errorString())
                ).show()
              }
            } catch (e: IOException) {
              snackbar(getString(R.string.status_report_failed_reason, e.message)).show()
            }
          }
        }
      } else {
        snackbar(R.string.status_report_failed).show()
      }
    } finally {
      binding.reportProgress.visibility = View.GONE
      binding.sendReportStatus.visibility = View.VISIBLE
    }
  }
}
