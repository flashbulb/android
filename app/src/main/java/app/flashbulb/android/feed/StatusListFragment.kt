package app.flashbulb.android.feed

import android.content.Context
import android.os.Bundle
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.work.WorkInfo
import androidx.work.WorkManager
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.databinding.StatusListBinding
import app.flashbulb.android.model.Status
import app.flashbulb.android.support.FlashbulbActivity
import app.flashbulb.android.support.FlashbulbFragment
import app.flashbulb.android.support.OnFragmentAttachedListener
import app.flashbulb.android.support.UrlHandler
import app.flashbulb.android.support.errorString
import com.google.android.material.snackbar.Snackbar
import either.fold
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class StatusListFragment : FlashbulbFragment() {
  override lateinit var rootLayout: View
  private var viewModel: StatusViewModel? = null
  private val viewModelStream = MutableLiveData<StatusViewModel>()
  private lateinit var viewAdapter: FeedAdapter
  private val gestureDetectorFactory = { listener: GestureDetector.OnGestureListener ->
    GestureDetector(context, listener)
  }

  private val urlHandler by lazy { UrlHandler(::startActivity) }
  private var _binding: StatusListBinding? = null
  private val binding get() = _binding!!

  override fun onAttach(context: Context) {
    super.onAttach(context)

    (context as? OnFragmentAttachedListener)?.onFragmentAttached(this)
    withApi(::populate)

    val statusInteraction = ViewModelProviders.of(this).get(StatusInteractionViewModel::class.java)

    statusInteraction.favListener.observe(this, buildObserver(::toggleFavorite))
    statusInteraction.boostListener.observe(this, buildObserver(::toggleBoost))
    statusInteraction.popupListener.observe(this, buildObserver(showPopup(context)))

    viewAdapter = FeedAdapter(
      statusInteraction,
      gestureDetectorFactory,
      fragmentManager!!,
      urlHandler
    )
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View =
    StatusListBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    rootLayout = binding.statusListLayout

    val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

    binding.entryList.adapter = viewAdapter
    binding.entryList.layoutManager = layoutManager
    binding.entryList.addItemDecoration(
      androidx.recyclerview.widget.DividerItemDecoration(
        binding.entryList.context,
        layoutManager.orientation
      )
    )

    (activity as? AppCompatActivity)?.setSupportActionBar(binding.toolbar)

    binding.takePhotoFab.show()
    binding.takePhotoFab.setImageResource(R.drawable.camera)
    binding.takePhotoFab.setOnClickListener {
      findNavController().navigate(R.id.action_statusListFragment_to_newStatusAlbumFragment)
    }

    binding.swipeEntryList.setOnRefreshListener {
      refresh()
    }

    viewModelStream.observe(viewLifecycleOwner, Observer { viewModel ->
      this.viewModel = viewModel

      viewModel.statuses.observe(
        this@StatusListFragment,
        Observer { pagedList ->
          viewAdapter.submitList(pagedList)
        }
      )

      viewModel.refreshState.observe(
        this@StatusListFragment,
        Observer<NetworkState> {
          binding.swipeEntryList.isRefreshing = it === NetworkState.Loading
        }
      )

      viewModel.networkState.observe(
        this@StatusListFragment,
        Observer { state ->
          when (state) {
            is NetworkState.Failed -> {
              val explanation = state.message?.let {
                getString(R.string.network_failed_reason, it)
              } ?: getString(R.string.network_failed)

              snackbar(explanation)
                .setAction(R.string.retry) { refresh() }
                .show()
            }
            is NetworkState.NoMoreTimeline -> {
              snackbar(R.string.no_more_timeline).show()
            }
            is NetworkState.AuthFailed -> {
              val explanation = state.message?.let {
                getString(R.string.auth_failed_reason, it)
              } ?: getString(R.string.auth_failed)

              snackbar(explanation).show()
            }
          }
        }
      )

      viewModel.load()
    })
  }

  private fun toggleFavorite(interaction: StatusInteraction) {
    toggleReaction(interaction) { vm -> interaction.status.toggleFavorite(vm::fav, vm::unfav) }
  }

  private fun toggleBoost(interaction: StatusInteraction) {
    if (interaction.status.isBoostable) {
      toggleReaction(interaction) { vm -> interaction.status.toggleBoost(vm::boost, vm::unboost) }
    }
  }

  /**
   * Flip the property on a status, such as a boost or a fav.
   *
   * This connects the API interaction with the UI by observing a Worker via
   * a ViewModel. The Worker is responsible for showing its own error
   * notifications.
   *
   * @param interaction how to relate the Status' state to the UI
   * @param toggle API communication
   */
  private fun toggleReaction(
    interaction: StatusInteraction,
    toggle: (StatusReactionViewModel) -> LiveData<WorkInfo>
  ) {
    interaction.onStart()

    lifecycleScope.launch(Dispatchers.Main) {
      val apiData = withContext(Dispatchers.Default) {
        (activity as? FlashbulbActivity)?.currentAccount()?.apiData()
      }

      apiData?.fold(
        { resId ->
          snackbar(resId).show()
          interaction.onError()
        },
        { apiDetails ->
          val vm = ViewModelProviders.of(this@StatusListFragment,
            object : ViewModelProvider.Factory {
              @Suppress("UNCHECKED_CAST") // StatusReactionViewModel is a ViewModel
              override fun <T : ViewModel?> create(modelClass: Class<T>): T =
                StatusReactionViewModel(
                  WorkManager.getInstance(requireContext()),
                  apiDetails.first,
                  apiDetails.second
                ) as T
            }
          )[StatusReactionViewModel::class.java]

          toggle(vm).observe(this@StatusListFragment, Observer { status ->
            when (status.state) {
              WorkInfo.State.SUCCEEDED -> interaction.onSuccess(status.outputData)
              WorkInfo.State.FAILED -> interaction.onError()
              else -> {
              }
            }
          })
        }
      ) ?: interaction.onError()
    }
  }

  /**
   * Show the popup menu. Curried so it can be combined with buildObserver.
   */
  private fun showPopup(context: Context) = { popup: StatusViewInteraction ->
    val popupMenu = PopupMenu(context, popup.view)
    popupMenu.setOnMenuItemClickListener { item -> onMenuItemClick(item, popup.status) }
    popupMenu.menuInflater.inflate(R.menu.status_menu, popupMenu.menu)
    popupMenu.show()
  }

  /**
   * Handle the selection from the popup menu for the specified status.
   *
   * This is implemented in this manner instead of via
   * OnMenuItemClickListener directly so we can thread the Status through the
   * calls.
   */
  private fun onMenuItemClick(item: MenuItem, status: Status) =
    when (item.itemId) {
      R.id.report -> {
        val args = Bundle().apply {
          putString(getString(R.string.account_id), status.account.id)
          putStringArrayList(getString(R.string.status_ids), arrayListOf(status.id))
        }

        findNavController().navigate(R.id.action_statusListFragment_to_reportStatusFragment, args)

        true
      }
      R.id.block -> {
        blockAccountDialog(status).show()
        true
      }
      R.id.mute -> {
        muteAccountDialog(status).show()
        true
      }
      else -> false
    }

  /**
   * Add to the appbar menu.
   */
  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.status_list_menu, menu)
  }

  /**
   * An item from the appbar menu was selected.
   */
  override fun onOptionsItemSelected(item: MenuItem) =
    when (item.itemId) {
      R.id.menu_refresh -> {
        refresh()
        true
      }
      R.id.menu_settings -> {
        findNavController()
          .navigate(R.id.action_statusListFragment_to_settingsFragment)
        true
      }
      else -> {
        super.onOptionsItemSelected(item)
      }
    }

  private fun populate(api: Api) {
    val vm = activity?.let {
      ViewModelProviders.of(
        it,
        object : ViewModelProvider.Factory {
          @Suppress("UNCHECKED_CAST") // StatusViewModel is a ViewModel
          override fun <T : ViewModel> create(modelClass: Class<T>): T =
            StatusViewModel(api) as T
        }
      )[StatusViewModel::class.java]
    } ?: return

    viewModelStream.postValue(vm)
  }

  private fun refresh() {
    viewModel?.refresh()
  }

  private fun blockAccountDialog(status: Status) =
    AlertDialog.Builder(context!!)
      .setTitle(R.string.block_account_title)
      .setMessage(getString(R.string.block_account_description, status.account.acct))
      .setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.cancel() }
      .setPositiveButton(R.string.block_account) { dialogInterface, _ ->
        dialogInterface.dismiss()

        withApi { api ->
          lifecycleScope.launch(Dispatchers.Main) {
            try {
              val result = withContext(Dispatchers.Default) {
                api.blockAccount(status.account.id).execute()
              }

              if (result.isSuccessful) {
                snackbar(R.string.account_blocked, duration = Snackbar.LENGTH_SHORT)
                  .show()
              } else {
                val msg = getString(
                  R.string.account_blocked_failed_reason,
                  result.errorString()
                )
                snackbar(msg).show()
              }
            } catch (e: IOException) {
              snackbar(
                getString(R.string.account_blocked_failed_reason, e.message)
              ).show()
            }
          }
        }
      }

  private fun muteAccountDialog(status: Status) =
    AlertDialog.Builder(context!!)
      .setTitle(R.string.mute_account_title)
      .setMessage(getString(R.string.mute_account_description, status.account.acct))
      .setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.cancel() }
      .setPositiveButton(R.string.mute_account) { dialogInterface, _ ->
        dialogInterface.dismiss()

        withApi { api ->
          lifecycleScope.launch(Dispatchers.Main) {
            try {
              val result = withContext(Dispatchers.Default) {
                api.muteAccount(status.account.id).execute()
              }

              if (result.isSuccessful) {
                snackbar(R.string.account_muted, duration = Snackbar.LENGTH_SHORT)
                  .show()
              } else {
                snackbar(
                  getString(R.string.account_muted_failed_reason, result.errorString())
                ).show()
              }
            } catch (e: IOException) {
              snackbar(
                getString(R.string.account_muted_failed_reason, e.message)
              ).show()
            }
          }
        }
      }

  private fun <T> buildObserver(f: (T) -> Any): Observer<T> =
    Observer { x -> x?.let { f(x) } }
}
