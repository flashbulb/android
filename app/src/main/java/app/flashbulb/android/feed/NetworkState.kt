package app.flashbulb.android.feed

sealed class NetworkState {
    object Loading : NetworkState()
    object Loaded : NetworkState()
    class Failed(val message: String? = null) : NetworkState()
    class AuthFailed(val message: String?) : NetworkState()
    object NoMoreTimeline : NetworkState()
}
