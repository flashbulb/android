package app.flashbulb.android.feed

import android.util.Log
import androidx.paging.PagedListAdapter
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DiffUtil
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.flashbulb.android.R
import app.flashbulb.android.model.Status
import app.flashbulb.android.support.UrlHandler
import app.flashbulb.android.ui.EntryViewHolder

class FeedAdapter(
        private val statusInteractionViewModel: StatusInteractionViewModel,
        private val gestureDetectorFactory: (GestureDetector.OnGestureListener) -> GestureDetector,
        private val fragmentManager: androidx.fragment.app.FragmentManager,
        private val urlHandler: UrlHandler
) : PagedListAdapter<Status, EntryViewHolder>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Status>() {
            override fun areItemsTheSame(oldEntry: Status, newEntry: Status) =
                    oldEntry.isTheSameAs(newEntry)

            override fun areContentsTheSame(oldEntry: Status, newEntry: Status) =
                    oldEntry == newEntry
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryViewHolder {
        val layout = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.status, parent, false)
                as ConstraintLayout

        return EntryViewHolder(
                layout,
                statusInteractionViewModel,
                gestureDetectorFactory,
                fragmentManager,
                urlHandler
        )
    }

    override fun onBindViewHolder(holder: EntryViewHolder, position: Int) {
        val entry = getItem(position)

        if (entry != null) {
            holder.bindTo(entry, View.generateViewId())
        } else {
            holder.clear()
        }
    }

    override fun onViewDetachedFromWindow(holder: EntryViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.pause()
    }
}
