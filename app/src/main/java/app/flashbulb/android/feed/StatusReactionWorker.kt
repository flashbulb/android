package app.flashbulb.android.feed

import android.content.Context
import androidx.annotation.StringRes
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.api.Status
import app.flashbulb.android.support.Notifier
import app.flashbulb.android.support.errorString
import retrofit2.Call
import java.io.IOException

class FavStatusWorker(context: Context, workerParameters: WorkerParameters) : StatusReactionWorker(context, workerParameters) {
    companion object {
        private const val KEY = "hasFavorited"
        fun fromWorkData(data: Data) = data.getBoolean(KEY, false)
    }

    override fun apiAction(api: Api, statusId: String) = api.favorite(statusId)
    override fun toWorkData(status: app.flashbulb.android.model.Status) =
            workDataOf(KEY to status.hasFavorited)

    override val failureReasonMsgId = R.string.favorite_failed_reason
    override val failureTitleId = R.string.fav_error_title
}

class UnfavStatusWorker(context: Context, workerParameters: WorkerParameters) : StatusReactionWorker(context, workerParameters) {
    companion object {
        private const val KEY = "hasFavorited"
        fun fromWorkData(data: Data) = data.getBoolean(KEY, false)
    }

    override fun apiAction(api: Api, statusId: String) = api.unfavorite(statusId)
    override fun toWorkData(status: app.flashbulb.android.model.Status) =
            workDataOf(KEY to status.hasFavorited)

    override val failureReasonMsgId = R.string.favorite_failed_reason
    override val failureTitleId = R.string.fav_error_title
}

class BoostStatusWorker(context: Context, workerParameters: WorkerParameters) : StatusReactionWorker(context, workerParameters) {
    companion object {
        private const val KEY = "hasBoosted"
        fun fromWorkData(data: Data) = data.getBoolean(KEY, false)
    }

    override fun apiAction(api: Api, statusId: String) = api.reblog(statusId)
    override fun toWorkData(status: app.flashbulb.android.model.Status) =
            workDataOf(KEY to status.hasBoosted)

    override val failureReasonMsgId = R.string.boost_failed_reason
    override val failureTitleId = R.string.boost_error_title
}

class UnboostStatusWorker(context: Context, workerParameters: WorkerParameters) : StatusReactionWorker(context, workerParameters) {
    companion object {
        private const val KEY = "hasBoosted"
        fun fromWorkData(data: Data) = data.getBoolean(KEY, false)
    }

    override fun apiAction(api: Api, statusId: String) = api.unreblog(statusId)
    override fun toWorkData(status: app.flashbulb.android.model.Status) =
            workDataOf(KEY to status.hasBoosted)

    override val failureReasonMsgId = R.string.boost_failed_reason
    override val failureTitleId = R.string.boost_error_title
}

abstract class StatusReactionWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {
    abstract fun apiAction(api: Api, statusId: String): Call<Status>
    abstract fun toWorkData(status: app.flashbulb.android.model.Status): Data
    abstract val failureReasonMsgId: Int
    abstract val failureTitleId: Int

    companion object {
        private const val STATUS_ID = "statusId"
        private const val BASE_URL = "baseUrl"
        private const val ACCESS_TOKEN = "accessToken"

        fun buildData(statusId: String, baseUrl: String, accessToken: String) =
                Data.Builder().apply {
                    putString(STATUS_ID, statusId)
                    putString(BASE_URL, baseUrl)
                    putString(ACCESS_TOKEN, accessToken)
                }.build()
    }

    override fun doWork(): Result {
        val data = reactionData()
        val api = Api.api(data.baseUrl, data.accessToken)

        try {
            val result = apiAction(api, data.statusId).execute()
            val newApiStatus = result.body()

            return if (result.isSuccessful && newApiStatus != null) {
                val newStatus = newApiStatus.toModel()
                Result.success(toWorkData(newStatus))
            } else {
                notify(data.statusId.toLong(), failureReasonMsgId, result.errorString())
                Result.failure()
            }
        } catch (e: IOException) {
            notify(data.statusId.toLong(), failureReasonMsgId, e.message)
            return Result.retry()
        }
    }

    private fun reactionData() =
            ReactionData(
                    inputData.getString(STATUS_ID)!!,
                    inputData.getString(BASE_URL)!!,
                    inputData.getString(ACCESS_TOKEN)!!
            )

    data class ReactionData(val statusId: String, val baseUrl: String, val accessToken: String)

    private val notifier by lazy { Notifier(applicationContext) }

    private fun notify(id: Long, @StringRes msgId: Int, arg: String? = null) {
        notifier.notify(
                id,
                getString(R.string.error_channel_name),
                getString(R.string.error_channel_description),
                getString(failureTitleId),
                getString(msgId, arg)
        )

        // TODO on click go to NewStatusAlbumFragment with draftId set appropriately
        //   TODO oh but auth issues should go elsewhere? I don't know what to do here
        // TODO add button to retry without opening app
        // TODO auto-retry when there is Internet
        //  TODO clear this specific notification if the retry succeeds
    }

    private fun getString(@StringRes msgId: Int, arg: String? = null) =
            if (arg == null)
                applicationContext.resources.getString(msgId)
            else
                applicationContext.resources.getString(msgId, arg)
}
