package app.flashbulb.android.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import app.flashbulb.android.api.Api
import app.flashbulb.android.model.Status

class StatusViewModel(api: Api) : ViewModel() {
    companion object {
        private const val PER_PAGE = 20
    }

    data class Feed<T>(
            val statuses: LiveData<PagedList<T>>,
            val refresh: () -> Unit,
            val refreshState: LiveData<NetworkState>,
            val networkState: LiveData<NetworkState>
    )

    sealed class Timeline {
        object Home : Timeline()
        /*
        object Local: Timeline()
        class Tag(val name: String): Timeline()
        ...
        */
    }

    private val sourceFactory = StatusDataSourceFactory(api)
    private val timeline = MutableLiveData<Timeline>()
    private val feed = map(timeline) {
        Feed(
                statuses = LivePagedListBuilder(
                        sourceFactory,
                        PER_PAGE
                ).build(),
                refresh = { sourceFactory.refresh() },
                refreshState = sourceFactory.refreshState,
                networkState = sourceFactory.networkState
        )
    }

    val statuses: LiveData<PagedList<Status>> = switchMap(feed) { it.statuses }
    val refreshState: LiveData<NetworkState> = switchMap(feed) { it.refreshState }
    val networkState: LiveData<NetworkState> = switchMap(feed) { it.networkState }

    fun load() {
        timeline.value = Timeline.Home
    }

    fun refresh() {
        feed.value?.refresh?.invoke()
    }
}
