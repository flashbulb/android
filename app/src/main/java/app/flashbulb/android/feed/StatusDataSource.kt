package app.flashbulb.android.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import app.flashbulb.android.api.Api
import app.flashbulb.android.api.Status
import app.flashbulb.android.support.Link
import app.flashbulb.android.support.errorString
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StatusDataSourceFactory(private val api: Api) : DataSource.Factory<String, app.flashbulb.android.model.Status>() {
    private val sourceLiveData = MutableLiveData<StatusDataSource>()

    override fun create(): DataSource<String, app.flashbulb.android.model.Status> =
            StatusDataSource(api).apply {
                sourceLiveData.postValue(this)
            }

    val refreshState: LiveData<NetworkState> = switchMap(sourceLiveData) { it.refreshState }
    val networkState: LiveData<NetworkState> = switchMap(sourceLiveData) { it.networkState }

    fun refresh() {
        sourceLiveData.value?.invalidate()
    }
}

class StatusDataSource(private val api: Api) : PageKeyedDataSource<String, app.flashbulb.android.model.Status>() {
    val refreshState = MutableLiveData<NetworkState>()
    val networkState = MutableLiveData<NetworkState>()

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, app.flashbulb.android.model.Status>) {
        networkState.postValue(NetworkState.Loading)

        api.homeTimeline(
                sinceId = params.key,
                limit = params.requestedLoadSize
        ).enqueue(
                object : Callback<List<Status>> {
                    override fun onFailure(call: Call<List<Status>>?, t: Throwable?) {
                        networkState.postValue(NetworkState.Failed(t?.localizedMessage))
                    }

                    override fun onResponse(call: Call<List<Status>>?, response: Response<List<Status>>) {
                        if (response.isSuccessful) {
                            val statuses = response.body() ?: listOf()
                            val linkHeader = response.headers()["Link"]
                            val prev = linkHeader?.let { linkHeaderQueryString(it, "prev", "since_id") }

                            callback.onResult(statuses.map { it.toModel() }, prev)
                            networkState.postValue(NetworkState.Loaded)
                        } else if (response.code() == 403) {
                            networkState.postValue(NetworkState.AuthFailed(response.errorString()))
                        } else {
                            networkState.postValue(NetworkState.Failed(response.errorString()))
                        }
                    }
                }
        )
    }

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, app.flashbulb.android.model.Status>) {
        networkState.postValue(NetworkState.Loading)
        refreshState.postValue(NetworkState.Loading)

        api.homeTimeline(limit = params.requestedLoadSize).enqueue(
                object : Callback<List<Status>> {
                    override fun onFailure(call: Call<List<Status>>?, t: Throwable?) {
                        networkState.postValue(NetworkState.Failed(t?.localizedMessage))
                        refreshState.postValue(NetworkState.Failed(t?.localizedMessage))
                    }

                    override fun onResponse(call: Call<List<Status>>?, response: Response<List<Status>>) {
                        if (response.isSuccessful) {
                            val statuses = response.body() ?: listOf()
                            val linkHeader = response.raw().header("Link")
                            val prev = linkHeader?.let { linkHeaderQueryString(it, "prev", "since_id") }
                            val next = linkHeader?.let { linkHeaderQueryString(it, "next", "max_id") }
                                    ?: statuses.lastOrNull()?.id

                            callback.onResult(statuses.map { it.toModel() }, prev, next)
                            networkState.postValue(NetworkState.Loaded)
                            refreshState.postValue(NetworkState.Loaded)
                        } else if (response.code() == 403) {
                            val msg = response.errorString()
                            networkState.postValue(NetworkState.AuthFailed(msg))
                            refreshState.postValue(NetworkState.AuthFailed(msg))
                        } else {
                            val msg = response.errorString()
                            networkState.postValue(NetworkState.Failed(msg))
                            refreshState.postValue(NetworkState.Failed(msg))
                        }
                    }
                }
        )
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, app.flashbulb.android.model.Status>) {
        networkState.postValue(NetworkState.Loading)

        api.homeTimeline(
                maxId = params.key,
                limit = params.requestedLoadSize
        ).enqueue(
                object : Callback<List<Status>> {
                    override fun onFailure(call: Call<List<Status>>?, t: Throwable?) {
                        networkState.postValue(NetworkState.Failed(t?.localizedMessage))
                    }

                    override fun onResponse(call: Call<List<Status>>?, response: Response<List<Status>>) {
                        if (response.isSuccessful) {
                            val statuses = response.body() ?: listOf()
                            val linkHeader = response.raw().header("Link")
                            val next = linkHeader?.let { linkHeaderQueryString(it, "next", "max_id") }
                                    ?: statuses.lastOrNull()?.id

                            if (next != null) {
                                if (statuses.isEmpty()) {
                                    networkState.postValue(NetworkState.Loaded)
                                    loadAfter(
                                            LoadParams(next, params.requestedLoadSize),
                                            callback
                                    )
                                } else {
                                    callback.onResult(statuses.map { it.toModel() }, next)
                                    networkState.postValue(NetworkState.Loaded)
                                }
                            } else {
                                networkState.postValue(NetworkState.NoMoreTimeline)
                            }
                        } else if (response.code() == 403) {
                            networkState.postValue(NetworkState.AuthFailed(response.errorString()))
                        } else {
                            networkState.postValue(NetworkState.Failed(response.errorString()))
                        }
                    }
                }
        )
    }

    private fun linkHeaderQueryString(linkHeader: String, relParam: String, queryKey: String): String? {
        val links: List<Link> = Link.valueOf(linkHeader)
        val link = links.find {
            it.params.any { param ->
                param.name == "rel" && param.values.contains(relParam)
            }
        }

        return link?.uri?.getQueryParameter(queryKey)
    }
}
