package app.flashbulb.android.feed

import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import app.flashbulb.android.R
import app.flashbulb.android.R.drawable
import app.flashbulb.android.api.MediaType
import app.flashbulb.android.databinding.AnImageBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

class MediaFragment : androidx.fragment.app.Fragment() {
  private lateinit var glide: RequestManager
  private val mediaItem by lazy { MediaItem.fromBundle(arguments!!) }
  private var videoPlayer: ExoPlayer? = null
  private var _binding: AnImageBinding? = null
  private val binding get() = _binding!!

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View =
    AnImageBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    if (!::glide.isInitialized)
      glide = Glide.with(this)

    if (mediaItem.isSensitive)
      bindSensitiveMedium()
    else
      bindMedium()
  }

  override fun onDestroyView() {
    videoPlayer?.release()
    glide.clear(binding.theImage)
    super.onDestroyView()
    _binding = null
  }

  fun pause() {
    videoPlayer?.playWhenReady = false
  }

  private fun bindSensitiveMedium() {
    showSensitivePlaceholder()

    binding.theImage.setOnClickListener {
      binding.theImage.visibility = View.GONE
      bindMedium()

      binding.theImage.postDelayed({
        glide.clear(binding.theImage)
        showSensitivePlaceholder()
      }, 5000)
    }
  }

  private fun bindMedium() {
    when (mediaItem.type) {
      MediaType.IMAGE -> bindImage()
      MediaType.VIDEO -> bindVideo()
      MediaType.GIFV -> bindVideo()
      MediaType.AUDIO -> bindVideo()
      MediaType.UNKNOWN -> bindUnknown()
    }
  }

  private fun showSensitivePlaceholder() {
    glide
      .applyDefaultRequestOptions(RequestOptions().dontTransform())
      .load(drawable.image_off)
      .into(binding.theImage)
    binding.theImage.contentDescription = null
    binding.theImage.visibility = View.VISIBLE
    binding.theImage.imageTintMode = PorterDuff.Mode.SRC_ATOP
  }

  private fun bindImage() {
    glide
      .applyDefaultRequestOptions(RequestOptions().dontTransform().fitCenter())
      .load(mediaItem.previewUrl) // TODO and eventually mediaItem.url ?
      .apply(
        RequestOptions().fallback(
          ContextCompat.getDrawable(
            requireContext(), drawable.cloud_alert
          )
        )
      )
      .into(binding.theImage)

    binding.theImage.contentDescription = mediaItem.description
    binding.theImage.visibility = View.VISIBLE
    binding.theImage.imageTintMode = PorterDuff.Mode.DST
  }

  private fun bindVideo() {
    val trackSelector = DefaultTrackSelector(null as BandwidthMeter?)
    val dataSourceFactory = DefaultDataSourceFactory(
      context, Util.getUserAgent(context, resources.getString(R.string.app_name)), null
    )
    val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
      .createMediaSource(Uri.parse(mediaItem.url))
    videoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector)

    videoPlayer?.let { player ->
      player.prepare(videoSource)
      player.repeatMode = Player.REPEAT_MODE_ALL
      binding.theVideo.player = player
      binding.theVideo.contentDescription = mediaItem.description
      binding.theVideo.visibility = View.VISIBLE
    }
  }

  private fun bindUnknown() {
    glide
      .applyDefaultRequestOptions(RequestOptions().dontTransform())
      .load(drawable.cloud_question)
      .into(binding.theImage)
    binding.theImage.contentDescription = resources.getString(R.string.unknown_media)
    binding.theImage.visibility = View.VISIBLE
    binding.theImage.imageTintMode = PorterDuff.Mode.SRC_ATOP
  }
}
