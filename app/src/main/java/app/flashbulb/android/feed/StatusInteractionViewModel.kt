package app.flashbulb.android.feed

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.view.View
import androidx.work.Data
import app.flashbulb.android.model.Status
import app.flashbulb.android.support.SingleLiveEvent
import app.flashbulb.android.ui.IconUpdater

class StatusInteractionViewModel : ViewModel() {
    private val _popupListener = SingleLiveEvent<StatusViewInteraction>()
    val popupListener: MutableLiveData<StatusViewInteraction>
        get() = _popupListener

    private val _favListener = SingleLiveEvent<StatusInteraction>()
    val favListener: MutableLiveData<StatusInteraction>
        get() = _favListener

    private val _boostListener = SingleLiveEvent<StatusInteraction>()
    val boostListener: MutableLiveData<StatusInteraction>
        get() = _boostListener
}

data class StatusViewInteraction(val status: Status, val view: View)
data class StatusInteraction(
        val status: Status,
        val onStart: () -> Unit,
        val onSuccess: (Data) -> Unit,
        val onError: () -> Unit
)
