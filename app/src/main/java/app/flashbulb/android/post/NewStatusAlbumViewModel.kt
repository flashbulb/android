package app.flashbulb.android.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import app.flashbulb.android.db.DraftMedia
import app.flashbulb.android.db.FlashbulbDao

class NewStatusAlbumViewModel(id: Long, dao: FlashbulbDao) : ViewModel() {
    val media: LiveData<PagedList<DraftMedia>> = LivePagedListBuilder(
            dao.mediaByDraftId(id), /* pageSize */ 20).build()
}
