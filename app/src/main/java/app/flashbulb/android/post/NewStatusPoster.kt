package app.flashbulb.android.post

import app.flashbulb.android.api.Api
import app.flashbulb.android.db.Draft
import app.flashbulb.android.db.DraftMedia
import app.flashbulb.android.db.DraftWithMedia
import app.flashbulb.android.support.errorString
import app.flashbulb.android.support.id
import either.Either
import either.Left
import either.Right
import either.fold
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

sealed class StatusResult {
    object MissingAttachmentBody : StatusResult()
    data class StatusFailed(val errorBody: String?) : StatusResult()
    object Success : StatusResult()
}

class NewStatusPoster(private val api: Api) {
    fun post(draft: DraftWithMedia): StatusResult =
            uploadMedia(draft.media).fold(::id, uploadStatus(draft.draft!!))

    private fun uploadMedia(media: List<DraftMedia>): Either<StatusResult, List<String>> {
        val attachmentIds = mutableListOf<String>()

        media.forEach { medium ->
            val attachmentResponse = api.uploadMedia(
                    filePart(medium.photoPath),
                    textPart(medium.description ?: "")
            ).execute()

            if (attachmentResponse.isSuccessful) {
                val attachment = attachmentResponse.body()
                        ?: return Left(StatusResult.MissingAttachmentBody)
                attachmentIds.add(attachment.id)
            } else {
                return Left(StatusResult.StatusFailed(attachmentResponse.errorString()))
            }
        }

        return Right(attachmentIds)
    }

    private fun uploadStatus(draft: Draft) = { attachmentIds: List<String> ->
        val statusResponse = api.toot(
                content = draft.caption ?: "",
                mediaId = attachmentIds,
                sensitive = draft.isSensitive,
                spoilerText = draft.cw ?: "",
                visibility = draft.visibility.toLowerCase(Locale.ROOT)
        ).execute()

        if (statusResponse.isSuccessful) {
            StatusResult.Success
        } else {
            StatusResult.StatusFailed(statusResponse.errorString())
        }
    }

    private fun filePart(path: String) =
            MultipartBody.Part.createFormData(
                    "file",
                    path,
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            File(path)
                    )
            )

    private fun textPart(str: String) =
            RequestBody.create(MediaType.parse("text/plain"), str)
}
