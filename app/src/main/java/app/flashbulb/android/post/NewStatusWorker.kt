package app.flashbulb.android.post

import android.content.Context
import androidx.annotation.StringRes
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.db.FlashbulbDatabase
import app.flashbulb.android.support.Notifier
import java.io.IOException

class NewStatusWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {
    companion object {
        private const val DRAFT_ID = "draftId"
        private const val BASE_URL = "baseUrl"
        private const val ACCESS_TOKEN = "accessToken"

        fun buildData(draftId: Long, baseUrl: String, accessToken: String) =
                Data.Builder().apply {
                    putLong(DRAFT_ID, draftId)
                    putString(BASE_URL, baseUrl)
                    putString(ACCESS_TOKEN, accessToken)
                }.build()
    }

    override fun doWork(): Result {
        val data = statusData()
        val api = Api.api(data.baseUrl, data.accessToken)
        val dao = FlashbulbDatabase.instance(applicationContext).dao()
        val draftWithMedia = dao.draftById(data.draftId)

        dao.updateDraft(draftWithMedia.draft!!)

        try {
            when (val result = NewStatusPoster(api).post(draftWithMedia)) {
                is StatusResult.MissingAttachmentBody -> {
                    notify(data.draftId, R.string.media_upload_failed)
                    return Result.failure()
                }
                is StatusResult.StatusFailed -> {
                    notify(data.draftId, R.string.status_post_failed_reason, result.errorBody)
                    return Result.failure()
                }
                is StatusResult.Success -> {
                    // TODO move this to a separate worker?
                    draftWithMedia.draft?.let { dao.deleteDraft(it) }
                    // TODO clean up the files
                    return Result.success()
                }
            }
        } catch (e: IOException) {
            notify(data.draftId, R.string.network_failed_reason, e.message)
            return Result.retry()
        }
    }

    private fun statusData() =
            StatusData(
                    inputData.getLong(DRAFT_ID, -1),
                    inputData.getString(BASE_URL)!!,
                    inputData.getString(ACCESS_TOKEN)!!
            )

    data class StatusData(val draftId: Long, val baseUrl: String, val accessToken: String)

    private val notifier by lazy { Notifier(applicationContext) }

    private fun notify(draftId: Long, @StringRes msgId: Int, arg: String? = null) {
        notifier.notify(
                draftId,
                getString(R.string.error_channel_name),
                getString(R.string.error_channel_description),
                getString(R.string.new_status_error_title),
                getString(msgId, arg)
        )

        // TODO on click go to NewStatusAlbumFragment with draftId set appropriately
        //   TODO oh but auth issues should go elsewhere? I don't know what to do here
        // TODO add button to retry without opening app
        // TODO auto-retry when there is Internet
        //  TODO clear this specific notification if the retry succeeds
    }

    private fun getString(@StringRes msgId: Int, arg: String? = null) =
            if (arg == null)
                applicationContext.resources.getString(msgId)
            else
                applicationContext.resources.getString(msgId, arg)
}
