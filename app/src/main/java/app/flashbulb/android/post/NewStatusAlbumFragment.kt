package app.flashbulb.android.post

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import app.flashbulb.android.R
import app.flashbulb.android.databinding.NewStatusAlbumBinding
import app.flashbulb.android.db.Draft
import app.flashbulb.android.db.DraftMedia
import app.flashbulb.android.db.FlashbulbDao
import app.flashbulb.android.db.FlashbulbDatabase
import app.flashbulb.android.support.FlashbulbFragment
import app.flashbulb.android.support.OnFragmentAttachedListener
import either.Either
import either.Left
import either.fold
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class NewStatusAlbumFragment : FlashbulbFragment() {
  override lateinit var rootLayout: View

  companion object {
    private const val FILE_PROVIDER_AUTHORITY = "app.flashbulb.android.fileprovider"
    private const val TAKE_PICTURE_REQUEST_CODE = 1
  }

  private var photoPath: String? = null
  private var draftId: Long? = null
  private var nextButtonEnabled = false
  private lateinit var dao: FlashbulbDao
  private lateinit var adapter: NewStatusAlbumAdapter
  private var _binding: NewStatusAlbumBinding? = null
  private val binding get() = _binding!!
  var createImageFile: () -> Either<Int, File> = {
    Left(R.string.file_io_exception)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)

    (context as? OnFragmentAttachedListener)?.onFragmentAttached(this)

    dao = FlashbulbDatabase.instance(context).dao()
    adapter = NewStatusAlbumAdapter()
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View =
    NewStatusAlbumBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    rootLayout = binding.newStatusAlbumLayout

    if (canTakePhotos()) {
      binding.openCameraFab.show()
      binding.openCameraFab.setImageResource(R.drawable.camera)
      binding.openCameraFab.setOnClickListener { takePhoto() }
    } else {
      binding.openCameraFab.hide()
    }

    connectViewModel(adapter)

    binding.draftMediaList.adapter = adapter
    binding.draftMediaList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

    (activity as? AppCompatActivity)?.setSupportActionBar(binding.newStatusAlbumToolbar)
  }

  private fun connectViewModel(adapter: NewStatusAlbumAdapter) {
    lifecycleScope.launch(Dispatchers.Main) {
      draftId = withContext(Dispatchers.Default) {
        if (draftId == null) {
          dao.insertDraft(Draft(0, System.currentTimeMillis()))
        } else {
          draftId
        }
      }

      val viewModel = ViewModelProviders.of(
        this@NewStatusAlbumFragment,
        object : ViewModelProvider.Factory {
          @Suppress("UNCHECKED_CAST") // NewStatusAlbumViewModel is a ViewModel
          override fun <T : ViewModel> create(modelClass: Class<T>): T =
            NewStatusAlbumViewModel(draftId!!, dao) as T
        }
      )[NewStatusAlbumViewModel::class.java]

      viewModel.media.observe(
        this@NewStatusAlbumFragment,
        Observer<PagedList<DraftMedia>> { pagedList ->
          if ((pagedList?.count() ?: 0) > 0) {
            nextButtonEnabled = true
            activity?.invalidateOptionsMenu()
            adapter.submitList(pagedList)

            if ((pagedList?.count() ?: 0) > 4) {
              snackbar(R.string.too_many_media).show()
            }
          } else {
            nextButtonEnabled = false
            activity?.invalidateOptionsMenu()
            takePhoto()
          }
        })
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if (requestCode == TAKE_PICTURE_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
      val args = Bundle().apply {
        putString(getString(R.string.photo_path), photoPath)
        putLong(getString(R.string.draft_id), draftId!!)
      }
      findNavController().navigate(R.id.action_newStatusAlbumFragment_to_photoEditFragment, args)
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.new_status_album, menu)
  }

  override fun onPrepareOptionsMenu(menu: Menu) {
    super.onPrepareOptionsMenu(menu)

    menu.findItem(R.id.done_with_new_album).isEnabled = nextButtonEnabled
  }

  override fun onOptionsItemSelected(item: MenuItem) =
    when (item.itemId) {
      R.id.done_with_new_album -> {
        val args = Bundle().apply {
          putLong(getString(R.string.draft_id), draftId!!)
        }

        findNavController().navigate(
          R.id.action_newStatusAlbumFragment_to_newStatusDetailsFragment,
          args
        )
        true
      }
      else -> super.onOptionsItemSelected(item)
    }

  private fun takePhoto() {
    createImageFile().fold({ snackbar(it).show() }, ::requestPhoto)
  }

  private fun requestPhoto(photoFile: File) {
    val photoURI = FileProvider.getUriForFile(
      context!!, FILE_PROVIDER_AUTHORITY, photoFile
    )
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
      putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
    }

    photoPath = photoFile.absolutePath
    startActivityForResult(intent, TAKE_PICTURE_REQUEST_CODE)
  }

  private fun canTakePhotos() =
    activity?.let {
      it.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) &&
          it.intent.resolveActivity(it.packageManager) != null
    } ?: false
}
