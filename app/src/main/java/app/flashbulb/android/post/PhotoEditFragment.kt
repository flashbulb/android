package app.flashbulb.android.post

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.flashbulb.android.R
import app.flashbulb.android.databinding.PhotoEditBinding
import app.flashbulb.android.db.DraftMedia
import app.flashbulb.android.db.FlashbulbDao
import app.flashbulb.android.db.FlashbulbDatabase
import app.flashbulb.android.support.OnFragmentAttachedListener
import app.flashbulb.android.support.hideKeyboard
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PhotoEditFragment : androidx.fragment.app.Fragment() {
  private lateinit var dao: FlashbulbDao
  private lateinit var glide: RequestManager
  private var draftId: Long? = null
  private var photoPath: String? = null
  private var _binding: PhotoEditBinding? = null
  private val binding get() = _binding!!

  override fun onAttach(context: Context?) {
    super.onAttach(context)
    (context as? OnFragmentAttachedListener)?.onFragmentAttached(this)
    dao = FlashbulbDatabase.instance(context!!).dao()
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View =
    PhotoEditBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    glide = Glide.with(this)
    photoPath = arguments?.getString(getString(R.string.photo_path))!!
    draftId = arguments?.getLong(getString(R.string.draft_id))!!

    glide.load(photoPath).into(binding.photoEditImagePreview)
    binding.doneWithPhotoEdit.setOnClickListener(::updateAndBackToList)
  }

  override fun onDestroyView() {
    glide.clear(binding.photoEditImagePreview)
    super.onDestroyView()
    _binding = null
  }

  private fun updateAndBackToList(view: View) {
    val description = binding.description.text.toString()
    hideKeyboard(activity, view)

    lifecycleScope.launch(Dispatchers.Main) {
      photoPath?.let { photoPath ->
        withContext(Dispatchers.Default) {
          dao.insertDraftMedia(DraftMedia(0, draftId!!, photoPath, description))
        }

        findNavController().popBackStack()
      }
    }
  }

}
