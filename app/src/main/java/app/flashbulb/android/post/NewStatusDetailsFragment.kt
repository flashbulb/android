package app.flashbulb.android.post

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.work.WorkManager
import app.flashbulb.android.R
import app.flashbulb.android.api.Visibility
import app.flashbulb.android.databinding.NewStatusDetailsBinding
import app.flashbulb.android.db.Draft
import app.flashbulb.android.db.DraftWithMedia
import app.flashbulb.android.db.FlashbulbDao
import app.flashbulb.android.db.FlashbulbDatabase
import app.flashbulb.android.feed.MediaPagerAdapter
import app.flashbulb.android.support.FlashbulbActivity
import app.flashbulb.android.support.FlashbulbFragment
import app.flashbulb.android.support.OnFragmentAttachedListener
import either.fold
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class NewStatusDetailsFragment : FlashbulbFragment() {
  override lateinit var rootLayout: View
  private lateinit var dao: FlashbulbDao
  private lateinit var draftWithMedia: DraftWithMedia
  private lateinit var adapter: MediaPagerAdapter
  private lateinit var newStatusViewModel: NewStatusViewModel
  private var _binding: NewStatusDetailsBinding? = null
  private val binding get() = _binding!!

  override fun onAttach(context: Context) {
    super.onAttach(context)
    (context as? OnFragmentAttachedListener)?.onFragmentAttached(this)
    dao = FlashbulbDatabase.instance(context).dao()
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    newStatusViewModel = NewStatusViewModel(
      WorkManager.getInstance(requireContext())
    )
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ) =
    NewStatusDetailsBinding.inflate(inflater, container, false).let {
      _binding = it
      binding.root
    }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    rootLayout = binding.newStatusDetailsLayout

    val draftId = arguments?.getLong(getString(R.string.draft_id))!!
    (activity as? AppCompatActivity)?.setSupportActionBar(binding.newStatusDetailsToolbar)

    binding.visibilityStatus.setOnClickListener { promptForVisibility() }

    lifecycleScope.launch(Dispatchers.Main) {
      draftWithMedia = withContext(Dispatchers.Default) {
        dao.draftById(draftId)
      }

      if (!::adapter.isInitialized)
        adapter = MediaPagerAdapter(fragmentManager!!, draftWithMedia.media, false)
      binding.mediaPreviewPager.adapter = adapter
      binding.pagerBullets.visibility = bulletVisibility(draftWithMedia.media.size)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.new_status_details_menu, menu)
  }

  override fun onOptionsItemSelected(item: MenuItem) =
    when (item.itemId) {
      R.id.done_with_new_status -> {
        doneWithStatus()
        true
      }
      else -> super.onOptionsItemSelected(item)
    }

  private fun promptForVisibility() {
    AlertDialog.Builder(context!!)
      .setTitle(R.string.select_visibility)
      .setItems(R.array.visibilities) { _, which ->
        val visibilityName = resources.getStringArray(R.array.visibilityMappings)[which]
        val vis = Visibility.valueOf(visibilityName)

        binding.visibilityStatus.setTag(R.id.new_status_visibility_value, vis.name)
        vis.iconUpdater.update(binding.visibilityStatus)
      }
      .setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.cancel() }
      .show()
  }

  private fun doneWithStatus() {
    draftWithMedia.draft?.let { draft ->
      lifecycleScope.launch(Dispatchers.Main) {
        val apiDetails = withContext(Dispatchers.Default) {
          (context as? FlashbulbActivity)?.currentAccount()?.apiData()
        }

        apiDetails?.fold(
          { resId -> snackbar(resId).show() },
          postNewStatus(draft)
        )
      }
    }
  }

  private fun postNewStatus(draft: Draft) = { apiDetails: Pair<String, String> ->
    draft.cw = binding.cw.text.toString()
    draft.isSensitive = binding.sensitive.isChecked
    draft.caption = binding.caption.text.toString()
    draft.visibility = visibility()

    lifecycleScope.launch(Dispatchers.Main) {
      withContext(Dispatchers.Default) {
        dao.updateDraft(draft)
      }

      newStatusViewModel.post(draft.id, apiDetails.first, apiDetails.second)
    }

    findNavController().popBackStack(R.id.statusListFragment, false)
  }

  private fun visibility() =
    Visibility.valueOf(
      binding.visibilityStatus.getTag(R.id.new_status_visibility_value) as String
    ).name

  private fun bulletVisibility(numberOfItems: Int) =
    if (numberOfItems < 2) View.GONE else View.VISIBLE
}
