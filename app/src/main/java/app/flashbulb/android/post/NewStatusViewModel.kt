package app.flashbulb.android.post

import androidx.lifecycle.ViewModel
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager

class NewStatusViewModel(private val workManager: WorkManager) : ViewModel() {
    companion object {
        private const val TAG = "NewStatusViewModel"
    }

    fun post(draftId: Long, baseUrl: String, accessToken: String) {
        val data = NewStatusWorker.buildData(draftId, baseUrl, accessToken)
        val statusRequest = OneTimeWorkRequestBuilder<NewStatusWorker>()
                .addTag(TAG)
                .setInputData(data)
                .build()

        workManager.enqueue(statusRequest)
    }
}
