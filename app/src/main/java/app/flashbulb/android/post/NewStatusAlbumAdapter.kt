package app.flashbulb.android.post

import androidx.paging.PagedListAdapter
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import app.flashbulb.android.R
import app.flashbulb.android.db.DraftMedia
import app.flashbulb.android.ui.DraftMediaViewHolder

class NewStatusAlbumAdapter : PagedListAdapter<DraftMedia, DraftMediaViewHolder>(
        object : DiffUtil.ItemCallback<DraftMedia>() {
            override fun areContentsTheSame(p0: DraftMedia, p1: DraftMedia) =
                    p0.id == p1.id

            override fun areItemsTheSame(p0: DraftMedia, p1: DraftMedia) =
                    p0 == p1
        }
) {
    override fun onBindViewHolder(viewHolder: DraftMediaViewHolder, position: Int) {
        val medium = getItem(position)
        if (medium == null) {
            viewHolder.clear()
        } else {
            viewHolder.bindTo(medium)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DraftMediaViewHolder {
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.draft_medium, parent, false)
                as ConstraintLayout

        return DraftMediaViewHolder(layout)
    }
}
