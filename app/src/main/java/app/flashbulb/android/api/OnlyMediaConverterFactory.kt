package app.flashbulb.android.api

import com.google.gson.GsonBuilder
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Convert the value to Gson as would be done in the GsonConverterFactory. If
 * the converted value is a List<Status>, filter out statuses without media.
 */
class OnlyMediaConverterFactory(private val base: Converter.Factory) : Converter.Factory() {
    override fun requestBodyConverter(type: Type, parameterAnnotations: Array<out Annotation>, methodAnnotations: Array<out Annotation>, retrofit: Retrofit): Converter<*, RequestBody>? {
        return base.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
    }

    override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit): Converter<ResponseBody, *> {
        val baseConverter = base.responseBodyConverter(type, annotations, retrofit)

        return Converter<ResponseBody, Any> { value ->
            var json = baseConverter!!.convert(value)

            if (isListOfStatus(type)) {
                json = (json as List<*>).filterIsInstance<Status>().filter { status ->
                    status.mediaAttachments.isNotEmpty() ||
                            status.reblog?.mediaAttachments?.isNotEmpty() == true
                }
            }

            json
        }
    }

    private fun isListOfStatus(type: Type) =
            type is ParameterizedType &&
                    type.rawType == java.util.List::class.java
                    && type.actualTypeArguments[0] == Status::class.java
}
