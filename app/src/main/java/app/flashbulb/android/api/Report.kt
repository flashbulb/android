package app.flashbulb.android.api

import com.google.gson.annotations.SerializedName

data class Report(
        @SerializedName("id") val id: String,
        @SerializedName("action_taken") val actionTaken: String
)
