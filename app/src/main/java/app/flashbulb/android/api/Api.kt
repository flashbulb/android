package app.flashbulb.android.api

import app.flashbulb.android.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface Api {
    companion object {
        fun api(baseUrl: String, accessToken: String? = null): Api =
                Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(OnlyMediaConverterFactory(gsonConverter))
                        .client(clientBuilder(accessToken).build())
                        .build()
                        .create(Api::class.java)

        private fun clientBuilder(accessToken: String?) =
                OkHttpClient.Builder().apply {
                    if (BuildConfig.DEBUG)
                        addInterceptor(HttpLoggingInterceptor().apply {
                            this.level = HttpLoggingInterceptor.Level.BODY
                        })

                    if (accessToken != null)
                        addInterceptor(BearerAuthenticationInterceptor(accessToken))
                }

        private val gson = GsonBuilder()
                .registerTypeAdapter(MediaType::class.java, MediaType.Deserializer())
                .create()

        private val gsonConverter = GsonConverterFactory.create(gson)
    }

    @FormUrlEncoded
    @POST("/oauth/token")
    fun requestAccessToken(
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String,
            @Field("code") code: String,
            @Field("redirect_uri") redirectUri: String,
            @Field("grant_type") grantType: String = "authorization_code"
    ): Call<AccessToken>

    @FormUrlEncoded
    @POST("/api/v1/apps")
    fun registerApp(
            @Field("client_name") clientName: String = "Flashbulb",
            @Field("redirect_uris") redirectUris: String = "flashbulb://flashbulb.app/oauth_redirect",
            @Field("scopes") scopes: String = "read write follow",
            @Field("website") website: String = "https://flashbulb.app/"
    ): Call<App>

    @GET("/api/v1/timelines/home")
    fun homeTimeline(
            @retrofit2.http.Query("max_id") maxId: String? = null,
            @retrofit2.http.Query("since_id") sinceId: String? = null,
            @retrofit2.http.Query("limit") limit: Int = 20
    ): Call<List<Status>>

    @Multipart
    @POST("/api/v1/media")
    fun uploadMedia(
            @Part file: MultipartBody.Part,
            @Part("description") description: RequestBody
            // @Part("focus") Focal point: Two floating points, comma-delimited
    ): Call<MediaAttachment>

    @FormUrlEncoded
    @POST("/api/v1/statuses")
    fun toot(
            @Field("status") content: String,
            @Field("media_ids[]") mediaId: List<String>,
            @Field("sensitive") sensitive: Boolean,
            @Field("spoiler_text") spoilerText: String,
            @Field("visibility") visibility: String
    ): Call<Status>

    @GET("/api/v1/accounts/verify_credentials")
    fun verifyCredentials(): Call<Account> // TODO Account + 'source' with account defaults

    @FormUrlEncoded
    @POST("/api/v1/reports")
    fun reportAccount(
            @Field("account_id") accountId: String,
            @Field("status_ids[]") statusIds: List<String>,
            @Field("comment") comment: String
    ): Call<Report>

    @POST("/api/v1/accounts/{account_id}/block")
    fun blockAccount(@Path("account_id") accountId: String): Call<Unit>

    @POST("/api/v1/accounts/{account_id}/mute")
    fun muteAccount(@Path("account_id") accountId: String): Call<Unit>

    @POST("/api/v1/statuses/{status_id}/favourite")
    fun favorite(@Path("status_id") statusId: String): Call<Status>

    @POST("/api/v1/statuses/{status_id}/unfavourite")
    fun unfavorite(@Path("status_id") statusId: String): Call<Status>

    @POST("/api/v1/statuses/{status_id}/reblog")
    fun reblog(@Path("status_id") statusId: String): Call<Status>

    @POST("/api/v1/statuses/{status_id}/unreblog")
    fun unreblog(@Path("status_id") statusId: String): Call<Status>
}
