package app.flashbulb.android.api

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import java.lang.IllegalStateException
import java.lang.reflect.Type
import java.util.*

enum class MediaType {
    @SerializedName("image")
    IMAGE,

    @SerializedName("video")
    VIDEO,

    @SerializedName("gifv")
    GIFV,

    @SerializedName("audio")
    AUDIO,

    @SerializedName("unknown")
    UNKNOWN;

    class Deserializer : JsonDeserializer<MediaType> {
        override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?) =
                try {
                    valueOf(json?.asString?.toUpperCase(Locale.ROOT) ?: "UNKNOWN")
                } catch (_: IllegalArgumentException) {
                    UNKNOWN
                } catch (_: ClassCastException) {
                    UNKNOWN
                } catch (_: IllegalStateException) {
                    UNKNOWN
                }
    }
}

interface PreviewableMedium {
    val url: String
    val previewUrl: String?
    val description: String?
    val type: MediaType
}

data class MediaAttachment(
        @SerializedName("id") val id: String,
        @SerializedName("type") override val type: MediaType,
        @SerializedName("url") override val url: String,
        @SerializedName("remote_url") val remoteUrl: String?,
        @SerializedName("preview_url") override val previewUrl: String?,
        @SerializedName("text_url") val textUrl: String?,
        //meta	See attachment metadata below	yes
        @SerializedName("description") override val description: String?
) : PreviewableMedium
