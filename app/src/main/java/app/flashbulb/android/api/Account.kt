package app.flashbulb.android.api

import com.google.gson.annotations.SerializedName

data class Account(
        @SerializedName("id") val id: String,
        @SerializedName("username") val username: String,
        @SerializedName("acct") val acct: String,
        @SerializedName("display_name") val displayName: String,
        @SerializedName("locked") val locked: Boolean,
        @SerializedName("created_at") val createdAt: String,
        @SerializedName("followers_count") val followersCount: Int,
        @SerializedName("following_count") val followingCount: Int,
        @SerializedName("statuses_count") val statusesCount: Int,
        @SerializedName("note") val note: String,
        @SerializedName("url") val url: String,
        @SerializedName("avatar") val avatar: String,
        @SerializedName("avatar_static") val avatarStatic: String,
        @SerializedName("header") val header: String,
        @SerializedName("header_static") val headerStatic: String
//        @SerializedName("moved") val moved: String? // TODO {"id":"322820","username":"mikeburns","acct":"mikeburns@octodon.social","display_name":"Mike Burns","locked":false,"bot":false,"created_at":"2018-04-10T01:52:13.971Z","note":"\u003cp\u003eMasc. body but done with masculinity. Pronoun: they.\u003c/p\u003e\u003cp\u003eProfessional inquiries: \u003cspan class=\"h-card\"\u003e\u003ca href=\"https://mastodon.technology/@mikeburns\" class=\"u-url mention\" rel=\"nofollow noopener\" target=\"_blank\"\u003e@\u003cspan\u003emikeburns\u003c/span\u003e\u003c/a\u003e\u003c/span\u003e\u003c/p\u003e\u003cp\u003eAbandoned account: \u003cspan class=\"h-card\"\u003e\u003ca href=\"https://mastodon.social/@mikeburns\" class=\"u-url mention\" rel=\"nofollow noopener\" target=\"_blank\"\u003e@\u003cspan\u003emikeburns\u003c/span\u003e\u003c/a\u003e\u003c/span\u003e\u003c/p\u003e","url":"https://octodon.social/@mikeburns","avatar":"https://files.mastodon.social/accounts/avatars/000/322/820/original/18a18bf315d49a26.jpg","avatar_static":"https://files.mastodon.social/accounts/avatars/000/322/820/original/18a18bf315d49a26.jpg","header":"https://mastodon.social/headers/original/missing.png","header_static":"https://mastodon.social/headers/original/missing.png","followers_count":117,"following_count":414,"statuses_count":157,"emojis":[],"fields":[]}
//    @SerializedName("fields") val fields: 	 	Array of profile metadata field, each element has 'name' and 'value' 	yes
)
