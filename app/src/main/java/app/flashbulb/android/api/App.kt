package app.flashbulb.android.api

import com.google.gson.annotations.SerializedName

data class App(
        @SerializedName("id") val id: String,
        @SerializedName("client_id") val clientId: String,
        @SerializedName("client_secret") val clientSecret: String
)