package app.flashbulb.android.api

import app.flashbulb.android.model.BoostedStatus
import app.flashbulb.android.model.SensitiveStatus
import app.flashbulb.android.model.SimpleStatus
import app.flashbulb.android.ui.IconUpdater
import app.flashbulb.android.ui.VisibilityIconUpdater
import com.google.gson.annotations.SerializedName

enum class Visibility(val isBoostable: Boolean, val iconUpdater: IconUpdater) {
    @SerializedName("public")
    PUBLIC(true, VisibilityIconUpdater.Public()),
    @SerializedName("unlisted")
    UNLISTED(true, VisibilityIconUpdater.Unlisted()),
    @SerializedName("private")
    PRIVATE(false, VisibilityIconUpdater.Private()),
    @SerializedName("direct")
    DIRECT(false, VisibilityIconUpdater.Direct()),
}

data class Status(
        @SerializedName("id") val id: String,
        @SerializedName("uri") val uri: String,
        @SerializedName("url") val url: String?,
        @SerializedName("account") val account: Account,
        @SerializedName("in_reply_to_id") val inReplyToId: Long?,
        @SerializedName("in_reply_to_account_id") val inReplyToAccountId: Long?,
        @SerializedName("reblog") val reblog: Status?,
        @SerializedName("content") val content: String,
        @SerializedName("created_at") val createdAt: String,
        // @SerializedName("emojis")	An array of Emoji	no
        @SerializedName("reblogs_count") val reblogsCount: Long,
        @SerializedName("favourites_count") val favoritesCount: Long,
        @SerializedName("reblogged") val hasReblogged: Boolean?,
        @SerializedName("favourited") val hasFavorited: Boolean?,
        @SerializedName("muted") val hasMuted: Boolean?,
        @SerializedName("sensitive") val sensitive: Boolean,
        @SerializedName("spoiler_text") val spoilerText: String,
        @SerializedName("visibility") val visibility: Visibility,
        @SerializedName("media_attachments") val mediaAttachments: List<MediaAttachment>,
        // @SerializedName("mentions")	An array of Mentions	no
        // @SerializedName("tags")	An array of Tags	no
        // @SerializedName("application") val application : Application?,
        @SerializedName("language") val language: String?,
        @SerializedName("pinned") val pinned: Boolean?
) {
    fun toModel(): app.flashbulb.android.model.Status =
            if (reblog?.mediaAttachments?.isNotEmpty() == true) {
                BoostedStatus(
                        id, uri, url, account, inReplyToId, inReplyToAccountId,
                        reblog.toModel(), content, createdAt, reblogsCount,
                        favoritesCount, hasReblogged == true,
                        hasFavorited == true, hasMuted == true, spoilerText,
                        visibility, mediaAttachments, language, pinned == true
                )
            } else if (sensitive) {
                SensitiveStatus(
                        id, uri, url, account, inReplyToId, inReplyToAccountId,
                        content, createdAt, reblogsCount, favoritesCount,
                        hasReblogged == true, hasFavorited == true,
                        hasMuted == true, spoilerText, visibility,
                        mediaAttachments, language, pinned == true
                )
            } else {
                SimpleStatus(
                        id, uri, url, account, inReplyToId, inReplyToAccountId,
                        content, createdAt, reblogsCount, favoritesCount,
                        hasReblogged == true, hasFavorited == true,
                        hasMuted == true, spoilerText, visibility,
                        mediaAttachments, language, pinned == true
                )
            }
}
