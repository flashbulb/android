package app.flashbulb.android.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import android.provider.BaseColumns

@Entity(indices = [Index(value = ["url"], unique = true), Index(value = ["accountName"], unique = true)])
data class Account(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = BaseColumns._ID) val id: Long,
        @ColumnInfo val accountId: String,
        @ColumnInfo val username: String,
        @ColumnInfo val acct: String,
        @ColumnInfo val displayName: String,
        @ColumnInfo val locked: Boolean,
        @ColumnInfo val createdAt: String,
        @ColumnInfo val followersCount: Int,
        @ColumnInfo val followingCount: Int,
        @ColumnInfo val statusesCount: Int,
        @ColumnInfo val note: String,
        @ColumnInfo val url: String,
        @ColumnInfo val avatar: String,
        @ColumnInfo val avatarStatic: String,
        @ColumnInfo val header: String,
        @ColumnInfo val headerStatic: String,
        @ColumnInfo val moved: String?,
        @ColumnInfo val accountName: String, // Full @acct@domain handle
        @ColumnInfo val baseUrl: String // The instance URL
) {
    constructor(json: app.flashbulb.android.api.Account, accountName: String, baseUrl: String) : this(
            id = 0,
            accountId = json.id,
            username = json.username,
            acct = json.acct,
            displayName = json.displayName,
            locked = json.locked,
            createdAt = json.createdAt,
            followersCount = json.followersCount,
            followingCount = json.followingCount,
            statusesCount = json.statusesCount,
            note = json.note,
            url = json.url,
            avatar = json.avatar,
            avatarStatic = json.avatarStatic,
            header = json.header,
            headerStatic = json.headerStatic,
            moved = null, // TODO
            accountName = accountName,
            baseUrl = baseUrl
    )
}
