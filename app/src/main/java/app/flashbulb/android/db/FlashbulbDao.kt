package app.flashbulb.android.db

import androidx.paging.DataSource
import androidx.room.*
import android.provider.BaseColumns

@Dao
interface FlashbulbDao {
    @Insert
    fun insertAccount(account: Account): Long

    @Query("SELECT * FROM Account WHERE accountName = :name")
    fun accountByName(name: String): Account?

    @Query("DELETE FROM Account WHERE accountName NOT IN (:names)")
    fun keepOnlyAccountsWithAccountNames(names: List<String>)

    @Insert
    fun insertDraft(draft: Draft): Long

    @Query("SELECT * FROM DraftMedia WHERE ${DraftMedia.DRAFT_ID} = :id")
    fun mediaByDraftId(id: Long): DataSource.Factory<Int, DraftMedia>

    @Insert
    fun insertDraftMedia(draftMedia: DraftMedia): Long

    @Transaction
    @Query("SELECT * from Draft WHERE ${BaseColumns._ID} = :id")
    fun draftById(id: Long): DraftWithMedia

    @Update
    fun updateDraft(draft: Draft): Int

    @Delete
    fun deleteDraft(draft: Draft): Int
}
