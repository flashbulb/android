package app.flashbulb.android.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.provider.BaseColumns
import app.flashbulb.android.api.Visibility

@Entity
data class Draft(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = BaseColumns._ID) val id: Long,
        @ColumnInfo val createdAt: Long,
        @ColumnInfo var cw: String? = null,
        @ColumnInfo var isSensitive: Boolean = true,
        @ColumnInfo var caption: String? = null,
        @ColumnInfo var visibility: String = Visibility.PUBLIC.name
)
