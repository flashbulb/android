package app.flashbulb.android.db

import androidx.room.Embedded
import androidx.room.Relation
import android.provider.BaseColumns

class DraftWithMedia {
    @Embedded var draft: Draft? = null
    @Relation(
            entity = DraftMedia::class,
            entityColumn = DraftMedia.DRAFT_ID,
            parentColumn = BaseColumns._ID
    ) var media: List<DraftMedia> = listOf()
}
