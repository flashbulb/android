package app.flashbulb.android.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context


@Database(entities = [Account::class, Draft::class, DraftMedia::class], version = 6, exportSchema = false)
abstract class FlashbulbDatabase : RoomDatabase() {
    abstract fun dao(): FlashbulbDao

    companion object {
        private var theInstance: FlashbulbDatabase? = null
        private const val DB_NAME = "flashbulb"

        fun instance(context: Context): FlashbulbDatabase {
            if (theInstance == null) {
                theInstance = Room
                        .databaseBuilder(
                                context.applicationContext,
                                FlashbulbDatabase::class.java,
                                DB_NAME
                        )
                        .addMigrations( /* https://developer.android.com/training/data-storage/room/migrating-db-versions */)
                        .build()
            }

            return theInstance!!
        }
    }
}
