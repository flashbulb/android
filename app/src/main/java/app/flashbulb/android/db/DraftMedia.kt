package app.flashbulb.android.db

import androidx.room.*
import android.provider.BaseColumns
import app.flashbulb.android.api.MediaType
import app.flashbulb.android.api.PreviewableMedium

@Entity(foreignKeys = [
    ForeignKey(
            entity = Draft::class,
            parentColumns = [(BaseColumns._ID)],
            childColumns = [DraftMedia.DRAFT_ID],
            onDelete = ForeignKey.CASCADE
    )])
data class DraftMedia(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = BaseColumns._ID) val id: Long,
        @ColumnInfo(index = true) val draftId: Long,
        @ColumnInfo val photoPath: String,
        @ColumnInfo override val description: String?
) : PreviewableMedium {
    @Ignore override val url: String = photoPath
    @Ignore override val previewUrl: String = photoPath
    @Ignore override val type: MediaType = MediaType.IMAGE

    companion object {
        const val DRAFT_ID = "draftId"
    }
}
