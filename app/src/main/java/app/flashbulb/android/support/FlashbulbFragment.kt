package app.flashbulb.android.support

import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import android.view.View
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import either.fold

abstract class FlashbulbFragment : androidx.fragment.app.Fragment() {
    abstract val rootLayout: View

    protected fun withApi(callback: (Api) -> Unit) {
        (context as? ApiProvider)?.withApi { potentialApi ->
            potentialApi.fold(
                    { msgId -> snackbar(msgId).show() },
                    callback
            )
        } ?: snackbar(R.string.lost_api).show()
    }

    protected fun snackbar(@StringRes resId: Int, duration: Int = Snackbar.LENGTH_LONG) =
            Snackbar.make(rootLayout, resId, duration)

    protected fun snackbar(msg: CharSequence, duration: Int = Snackbar.LENGTH_LONG) =
            Snackbar.make(rootLayout, msg, duration)
}
