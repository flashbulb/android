package app.flashbulb.android.support

import android.content.Intent
import android.net.Uri

class UrlHandler(private val startActivity: (Intent) -> Unit) {
    fun openExternalUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    fun openInternalUrl(url: String) {
        // TODO launch an appropriate activity for handling the URL
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }
}
