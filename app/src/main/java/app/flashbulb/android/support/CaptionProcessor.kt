package app.flashbulb.android.support

import android.graphics.Typeface
import android.text.style.ClickableSpan
import android.text.style.StyleSpan
import android.text.style.UpdateAppearance
import android.view.View
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode

class CaptionProcessor(private val caption: String, private val urlHandler: UrlHandler) {
    private val doc by lazy { Jsoup.parseBodyFragment(caption).body() }

    fun spannables(): Iterable<SpannedText> =
            doc.select("p").foldIndexed(listOf()) { i, acc, p ->
                if (i == 0)
                    acc + processP(p)
                else
                    acc + SpannedText("\n\n", PlainTextSpan()) + processP(p)
            }

    private fun processP(e: Element): List<SpannedText> =
            if (isEmptyP(e) || isOnlyDotP(e))
                listOf()
            else
                e.childNodes().map(::process)

    private fun isEmptyP(e: Element) =
            e.childNodes().size == 0

    private fun isOnlyDotP(e: Element) =
            e.childNodes().let { children ->
                children.size == 1 &&
                        (children[0] as? TextNode)?.text() == "."
            }

    private fun process(n: Node): SpannedText =
            when (n.nodeName()) {
                "#text" -> SpannedText((n as TextNode).text(), PlainTextSpan())
                "br" -> SpannedText("\n", PlainTextSpan())
                "a" -> processA(n as Element)
                "span" -> {
                    if (isVCard(n as Element))
                        process(n.childNode(0))
                    else
                        SpannedText(n.text(), PlainTextSpan())
                }
                else -> {
                    SpannedText(
                            "unexpected HTML: ${n.nodeName()}",
                            StyleSpan(Typeface.BOLD_ITALIC)
                    )
                }
            }

    private fun processA(e: Element): SpannedText =
            if (isMastodonExternalA(e)) {
                val textChild = e.children()[1]
                val text = textChild.text()
                val isEllipsis = textChild.classNames().contains("ellipsis")

                if (isEllipsis) {
                    SpannedText("$text…", textLinkExternal(e.attr("href")))
                } else {
                    SpannedText(text, textLinkExternal(e.attr("href")))
                }
            } else if (isMastodonInternalA(e)) {
                SpannedText(e.text(), textLinkInternal(e.attr("href")))
            } else {
                SpannedText(e.text(), textLinkExternal(e.attr("href")))
            }

    private fun isMastodonExternalA(e: Element) =
            e.childNodes().let { children ->
                children.size == 3 &&
                        children.all { child ->
                            (child as? Element)?.tagName() == "span"
                        }
            }

    private fun isMastodonInternalA(e: Element) =
            e.classNames().contains("u-url") &&
                    e.childNodeSize() == 2

    private fun isVCard(e: Element) =
            e.classNames().contains("h-card")

    private fun textLinkExternal(url: String) =
            object : ClickableSpan() {
                override fun onClick(widget: View) {
                    urlHandler.openExternalUrl(url)
                }
            }

    private fun textLinkInternal(url: String) =
            object : ClickableSpan() {
                override fun onClick(widget: View) {
                    urlHandler.openInternalUrl(url)
                }
            }
}
