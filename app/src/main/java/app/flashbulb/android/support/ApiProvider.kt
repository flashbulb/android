package app.flashbulb.android.support

import app.flashbulb.android.api.Api
import either.Either

interface ApiProvider {
    fun withApi(callback: (Either<Int, Api>) -> Unit)
}
