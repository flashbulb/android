package app.flashbulb.android.support

import android.accounts.*
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.auth.AuthActivity
import app.flashbulb.android.auth.Authenticator
import app.flashbulb.android.db.FlashbulbDao
import app.flashbulb.android.db.FlashbulbDatabase
import either.Either
import either.Left
import either.Right
import either.map

abstract class FlashbulbActivity : AppCompatActivity(), Themeable {
    companion object {
        const val PREF_BASE_URL = "baseUrl"
        const val OAUTH_PREFERENCES_FILE = "app.flashbulb.android.OAUTH_PREFERENCE_FILE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        setTheme(themeMapping(themeFromPreferences()))
    }

    override fun themeMapping(themeName: String): Int =
            when (themeName) {
                getString(R.string.light_theme_value) -> R.style.LightTheme
                else -> R.style.DarkTheme
            }

    val oauthPrefs: SharedPreferences by lazy {
        getSharedPreferences(OAUTH_PREFERENCES_FILE, Context.MODE_PRIVATE)
    }

    val accountManager by lazy {
        AccountManager.get(applicationContext)!!
    }

    fun accounts(): Array<Account> {
        return accountManager.getAccountsByType(Authenticator.ACCOUNT_TYPE)
    }

    val dao by lazy {
        FlashbulbDatabase.instance(applicationContext).dao()
    }

    fun currentAccount(): CurrentAccount {
        var accountName = oauthPrefs.getString(AuthActivity.ACCOUNT_NAME, "")
        var account = accounts().find { it.name == accountName }

        while (account == null) {
            accountManager.getAuthToken(
                    Account(Authenticator.ACCOUNT_TYPE, Authenticator.ACCOUNT_TYPE),
                    Authenticator.ACCOUNT_TYPE, null, this, null, null).result
            accountName = oauthPrefs.getString(AuthActivity.ACCOUNT_NAME, "")
            account = accounts().find { it.name == accountName }
        }

        val future = accountManager.getAuthToken(
                account, Authenticator.ACCOUNT_TYPE, null, this, null, null)

        return CurrentAccount(future, dao, account)
    }

    private val preferenceManager by lazy { PreferenceManager.getDefaultSharedPreferences(this) }
    private fun themeFromPreferences() =
            preferenceManager.getString(
                    getString(R.string.pref_key_theme),
                    getString(R.string.light_theme_value)
            )!!

    class CurrentAccount(
            private val accountFuture: AccountManagerFuture<Bundle>,
            private val dao: FlashbulbDao,
            val account: Account) {

        private val dbAccount: app.flashbulb.android.db.Account? by lazy {
            dao.accountByName(account.name)
        }

        fun apiData(): Either<Int, Pair<String, String>> =
                try {
                    val accessToken = accountFuture.result?.getString(AccountManager.KEY_AUTHTOKEN)
                    if (dbAccount != null && accessToken != null) {
                        Right(dbAccount?.baseUrl!! to accessToken)
                    } else {
                        Left(R.string.data_integrity_failure)
                    }
                } catch (e: OperationCanceledException) {
                    Left(R.string.operation_canceled)
                } catch (e: AuthenticatorException) {
                    Left(R.string.authenticator_failed)
                }

        fun api(): Either<Int, Api> =
                apiData().map { Api.api(it.first, it.second) }
    }
}
