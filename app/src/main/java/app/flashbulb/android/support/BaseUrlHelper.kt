package app.flashbulb.android.support

import app.flashbulb.android.R
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException


/**
 * Turn a user-entered instance URL into a full URL with a schema.
 *
 * 1. If it's blank, abort.
 * 2. If there's no schema, start with "https".
 * 3. Try a HEAD request.
 * 4. If that succeeds, finish.
 * 5. If the HEAD request fails for HTTP reasons (with a response code), abort.
 * 6. If the HEAD request fails for TCP reasons (e.g. timeout, TLS handshake),
 * and the user manually input the schema (we did not set it in (2)), abort.
 * 7. If HEAD request failed and we set schema in (2), set it to "http" instead.
 * 8. Try a HEAD request.
 * 9. If that fails at all, abort.
 * 10. If that succeeds, finish.
 *
 * @param [instanceUrlFromUser] the user-entered instance URL
 * @param [fail] callback to run on error, passing the resource ID for
 * the error string and an optional detailed explanation
 * @param [success] callback to run on success, passing the new URL
 */
fun normalizedBaseUrl(instanceUrlFromUser: String, fail: (Int, String?) -> Unit, success: (String) -> Unit) {
    var baseUrl = instanceUrlFromUser
    var didSetSchema = false

    if (instanceUrlFromUser.isBlank()) {
        fail(R.string.blank_hostname, null)
        return
    }

    if (!instanceUrlFromUser.regionMatches(thisOffset = 0, other = "http", otherOffset = 0, length = 4, ignoreCase = true)) {
        baseUrl = "https://$instanceUrlFromUser"
        didSetSchema = true
    }

    val client = OkHttpClient()
    val req = Request.Builder()
            .url(baseUrl)
            .head()
            .build()

    try {
        val response = client.newCall(req).execute()

        if (response.isSuccessful) {
            success(baseUrl)
        } else {
            fail(R.string.network_failed_code, response.code().toString())
        }
    } catch (e: IOException) {
        if (didSetSchema) {
            baseUrl = "http://$instanceUrlFromUser"
            val httpReq = Request.Builder()
                    .url(baseUrl)
                    .head()
                    .build()
            try {
                val httpResponse = client.newCall(httpReq).execute()
                if (httpResponse.isSuccessful) {
                    success(baseUrl)
                } else {
                    fail(R.string.network_failed_code, httpResponse.code().toString())
                }
            } catch (e: IOException) {
                fail(R.string.network_failed_reason, e.message)
            }
        } else {
            fail(R.string.network_failed_reason, e.message)
        }
    }
}
