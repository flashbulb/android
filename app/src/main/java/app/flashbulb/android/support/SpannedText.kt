package app.flashbulb.android.support

data class SpannedText(val text: String, val what: Any)
