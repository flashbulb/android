package app.flashbulb.android.support

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.flashbulb.android.R

class Notifier(private val context: Context) {
    companion object {
        private const val ERROR_CHANNEL = "errors"
    }

    fun notify(id: Long, channelName: CharSequence, channelDescr: String, title: CharSequence, msg: CharSequence) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(ERROR_CHANNEL, channelName, NotificationManager.IMPORTANCE_DEFAULT).apply {
                description = channelDescr
            }
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context, ERROR_CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(msg)
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
                .setCategory(NotificationCompat.CATEGORY_ERROR)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        NotificationManagerCompat.from(context).notify(id.toInt(), builder.build())
    }
}
