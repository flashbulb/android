package app.flashbulb.android.support

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.annotations.SerializedName
import retrofit2.Response

/**
 * Parse the response body into an error message. Mastodon sends them in the
 * form of:
 *
 *     { "error": "the message here" }
 *
 * If we fail to parse it -- because it's not JSON in that format -- then
 * return the body string as it, if there even is one.
 *
 * @return the extracted error message, or the response body, or `null`.
 */
fun <T> Response<T>.errorString(): String? =
        this.errorBody()?.string()?.let { errorJson ->
            try {
                Gson().fromJson<ErrorResponse>(errorJson, ErrorResponse::class.java)?.error
            } catch (e: JsonSyntaxException) {
                errorJson
            }
        }

private data class ErrorResponse(@SerializedName("error") val error: String)
