package app.flashbulb.android.support

import android.net.Uri
import com.github.h0tk3y.betterParse.combinators.*
import com.github.h0tk3y.betterParse.grammar.Grammar
import com.github.h0tk3y.betterParse.grammar.tryParseToEnd
import com.github.h0tk3y.betterParse.parser.Parsed
import com.github.h0tk3y.betterParse.parser.Parser

/**
 * Parser for RFC 5988 "Link" HTTP header.
 *
 * An example input might look like:
 * <http://10.0.2.2:3000/api/v1/timelines/home?limit=120&max_id=100060092826538005>; rel="next", <http://10.0.2.2:3000/api/v1/timelines/home?limit=120&since_id=100106017415261738>; rel="prev"
 *
 * This parser is not as rigorous as it could be. Such is life.
 */
data class Link(val uri: Uri, val params: List<Param>) {
    data class Param(val name: String, val values: List<String>)

    companion object {
        /**
         * Turn a header value into a list of Link objects.
         *
         * This method is named in honor of javax.ws.rs.core.Link,
         * which might have done what I wanted but also brought in
         * three kitchen sinks.
         */
        fun valueOf(linkStr: String): List<Link> {
            val linkGrammar = object : Grammar<List<Link>>() {
                val comma by token(",")
                val equal by token("=")
                val semi by token(";")
                val parmNameExtOrValue by token("\\b[-a-zA-Z0-9!#$&+.^_`|~*]+\\b")
                val doubleQuote by token("\"")
                val uriReference by token("<[^>]+>")

                val uriParser by uriReference map { uriString ->
                    Uri.parse(uriString.text.removeSurrounding("<", ">"))
                }
                val parmNameParser by parmNameExtOrValue map { it.text }
                val paramValueParser
                        by (parmNameParser map { name -> listOf(name) }) or
                                (skip(doubleQuote) * parmNameParser * zeroOrMore(parmNameParser) * skip(doubleQuote) map { (name, names) ->
                                    listOf(name) + names
                                })
                val linkParam by parmNameParser * skip(equal) * paramValueParser map { (name, values) ->
                    Param(name, values)
                }
                val commaParser by comma

                val linkValue
                        by uriParser *
                                zeroOrMore(skip(semi) * linkParam) map { (uri, rels) ->
                            Link(uri, rels)
                        }

                val linkValues by separatedTerms(linkValue, commaParser)
                override val rootParser: Parser<List<Link>> by linkValues
            }

            val result = linkGrammar.tryParseToEnd(linkStr)
            return when (result) {
                is Parsed<List<Link>> -> result.value
                else -> listOf()
            }
        }
    }
}
