package app.flashbulb.android.support

interface Themeable {
    fun themeMapping(themeName: String): Int
}
