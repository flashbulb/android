package app.flashbulb.android.support

import androidx.fragment.app.Fragment

interface OnFragmentAttachedListener {
    fun onFragmentAttached(fragment: androidx.fragment.app.Fragment)
}
