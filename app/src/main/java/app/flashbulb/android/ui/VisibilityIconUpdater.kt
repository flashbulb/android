package app.flashbulb.android.ui

import android.widget.ImageView
import app.flashbulb.android.R

abstract class VisibilityIconUpdater : IconUpdater {
    override fun update(view: ImageView) {
        view.setImageResource(drawable)
        view.contentDescription =
                view.context.resources.getString(description)
    }

    protected abstract val drawable: Int
    protected abstract val description: Int

    class Public : VisibilityIconUpdater() {
        override val drawable = R.drawable.earth
        override val description = R.string.public_status
    }

    class Unlisted : VisibilityIconUpdater() {
        override val drawable = R.drawable.lock_open
        override val description = R.string.unlisted_status
    }

    class Private : VisibilityIconUpdater() {
        override val drawable = R.drawable.lock
        override val description = R.string.private_status
    }

    class Direct : VisibilityIconUpdater() {
        override val drawable = R.drawable.email
        override val description = R.string.direct_status
    }
}
