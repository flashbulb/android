package app.flashbulb.android.ui

import android.widget.ImageView
import app.flashbulb.android.R

abstract class FavoriteIconUpdater : IconUpdater {
    companion object {
        fun build(hasFavorited: Boolean): FavoriteIconUpdater =
                if (hasFavorited)
                    FavoriteIconUpdater.Favorite()
                else
                    FavoriteIconUpdater.NotFavorite()
    }

    override fun update(view: ImageView) {
        view.setImageResource(drawable)
        view.contentDescription =
                view.context.resources.getString(description)
    }

    protected abstract val drawable: Int
    protected abstract val description: Int

    class Favorite : FavoriteIconUpdater() {
        override val drawable = R.drawable.heart
        override val description = R.string.favorited
    }

    class NotFavorite : FavoriteIconUpdater() {
        override val drawable = R.drawable.heart_outline
        override val description = R.string.not_favorited
    }
}
