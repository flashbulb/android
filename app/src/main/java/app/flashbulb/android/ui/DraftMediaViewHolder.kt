package app.flashbulb.android.ui

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import app.flashbulb.android.R
import app.flashbulb.android.db.DraftMedia
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

class DraftMediaViewHolder(private val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    private val imageView by lazy { view.findViewById<ImageView>(R.id.draft_medium_thumbnail) }
    private val description by lazy { view.findViewById<TextView>(R.id.draft_medium_description) }
    private val glide: RequestManager by lazy { Glide.with(view) }

    fun bindTo(medium: DraftMedia) {
        glide.load(medium.photoPath).into(imageView)
        description.text = medium.description
    }

    fun clear() {
        glide.clear(imageView)
    }
}
