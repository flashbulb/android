package app.flashbulb.android.ui

import android.annotation.SuppressLint
import android.graphics.Typeface
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.text.style.UpdateAppearance
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import app.flashbulb.android.R
import app.flashbulb.android.api.Account
import app.flashbulb.android.api.MediaAttachment
import app.flashbulb.android.api.PreviewableMedium
import app.flashbulb.android.feed.*
import app.flashbulb.android.model.Status
import app.flashbulb.android.support.CaptionProcessor
import app.flashbulb.android.support.UrlHandler
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

class EntryViewHolder(
        val layout: View,
        private val statusInteractionViewModel: StatusInteractionViewModel,
        private val gestureDetectorFactory: (GestureDetector.OnGestureListener) -> GestureDetector,
        private val fragmentManager: androidx.fragment.app.FragmentManager,
        private val urlHandler: UrlHandler
) : RecyclerView.ViewHolder(layout) {
    private val statusDescription: TextView = layout.findViewById(R.id.status_description)
    private val avatar: ImageView = layout.findViewById(R.id.account_avatar)
    private val boosterAvatar: ImageView = layout.findViewById(R.id.booster_avatar)
    private val popupMenuIcon: ImageView = layout.findViewById(R.id.popup_menu)
    private val imagePager: ViewPager = layout.findViewById(R.id.image_pager)
    private val pagerBullets: TabLayout = layout.findViewById(R.id.pager_bullets)
    private val favoriteStatus: ImageView = layout.findViewById(R.id.favorite_status)
    private val boostStatus: ImageView = layout.findViewById(R.id.boost_status)
    private val visibilityStatus: ImageView = layout.findViewById(R.id.visibility_status)
    private val glide: RequestManager by lazy { Glide.with(layout) }

    fun bindTo(status: Status, id: Int) {
        imagePager.id = id
        status.bind(this)
    }

    fun clear() {
        statusDescription.text = null
        glide.clear(avatar)

        clearBoosterAccountName()
    }

    fun pause() {
        (imagePager.adapter as MediaPagerAdapter).pause()
    }

    @SuppressLint("ClickableViewAccessibility") // fav functionality click via heart icon
    fun bindActions(status: Status) {
        popupMenuIcon.setOnClickListener { view ->
            statusInteractionViewModel.popupListener.value =
                    StatusViewInteraction(status, view)
        }

        val gestureDetector = gestureDetectorFactory(doubleTapGesture {
            toggleFav(status)
        })
        imagePager.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
        }
        favoriteStatus.setOnClickListener { toggleFav(status) }

        boostStatus.setOnClickListener { toggleBoost(status) }
    }

    fun bindMedia(mediaAttachments: List<MediaAttachment>) {
        bindMedia(mediaAttachments, sensitive = false)
    }

    fun bindSensitiveMedia(mediaAttachments: List<MediaAttachment>) {
        bindMedia(mediaAttachments, sensitive = true)
    }

    fun bindDescription(account: Account, cw: String, caption: String) {
        statusDescription.text = buildDescription(account, cw, caption)
        statusDescription.movementMethod = LinkMovementMethod.getInstance()
    }

    fun bindAccountAvatar(account: Account) {
        glide.load(account.avatar).into(avatar)
    }

    fun bindBoosterAccountName(account: Account) {
        boosterAvatar.contentDescription = account.acct

        glide.load(account.avatar).into(boosterAvatar)
    }

    fun bindVisibility(iconUpdater: IconUpdater) {
        iconUpdater.update(visibilityStatus)
    }

    fun bindFavoriteStatus(iconUpdater: IconUpdater) {
        iconUpdater.update(favoriteStatus)
    }

    fun bindBoostStatus(iconUpdater: IconUpdater) {
        iconUpdater.update(boostStatus)
    }

    fun clearBoosterAccountName() {
        boosterAvatar.contentDescription = null
        glide.clear(boosterAvatar)
    }

    private fun bindMedia(mediaAttachments: List<PreviewableMedium>, sensitive: Boolean) {
        imagePager.adapter = MediaPagerAdapter(fragmentManager, mediaAttachments, sensitive)
        pagerBullets.visibility = bulletVisibility(mediaAttachments.size)
    }

    private fun bulletVisibility(numberOfItems: Int) =
            if (numberOfItems < 2) View.INVISIBLE else View.VISIBLE

    private fun toggleFav(status: Status) {
        statusInteractionViewModel.favListener.value =
                StatusInteraction(
                        status = status,
                        onStart = {
                            updateFavoriteStatus(R.drawable.heart_pulse, R.string.favoriting)
                        },
                        onError = {
                            updateFavoriteStatus(R.drawable.heart_broken, R.string.favorite_failed)
                        },
                        onSuccess = { data ->
                            status.hasFavorited = FavStatusWorker.fromWorkData(data)
                            bindFavoriteStatus(status.favoriteIconUpdater())
                        }
                )
    }

    private fun toggleBoost(status: Status) {
        statusInteractionViewModel.boostListener.value =
                StatusInteraction(
                        status = status,
                        onStart = {
                            updateBoostStatus(R.drawable.reload, R.string.boosting)
                        },
                        onError = {
                            updateBoostStatus(R.drawable.sync_alert, R.string.boost_failed)
                        },
                        onSuccess = { data ->
                            status.hasBoosted = BoostStatusWorker.fromWorkData(data)
                            bindBoostStatus(status.boostIconUpdater())
                        }
                )
    }

    private fun updateFavoriteStatus(@DrawableRes drawable: Int, @StringRes description: Int) {
        favoriteStatus.setImageResource(drawable)
        favoriteStatus.contentDescription =
                favoriteStatus.context.resources.getString(description)
    }

    private fun updateBoostStatus(@DrawableRes drawable: Int, @StringRes description: Int) {
        boostStatus.setImageResource(drawable)
        boostStatus.contentDescription =
                boostStatus.context.resources.getString(description)
    }

    private fun doubleTapGesture(f: (MotionEvent?) -> Unit) =
            object : GestureDetector.SimpleOnGestureListener() {
                override fun onDown(e: MotionEvent?) = true
                override fun onDoubleTap(e: MotionEvent?): Boolean {
                    f(e)
                    return true
                }
            }

    private fun buildDescription(account: Account, cw: String, caption: String): Spannable {
        val descr = SpannableStringBuilder()

        descr.append("${account.acct} ", StyleSpan(Typeface.BOLD), 0)
        descr.setSpan(
                undecoratedClickable { urlHandler.openInternalUrl(account.url) },
                0, account.acct.length, 0
        )

        if (cw.isBlank()) {
            CaptionProcessor(caption, urlHandler).spannables().forEach { spannable ->
                descr.append(spannable.text, spannable.what, 0)
            }
        } else {
            val offset = account.acct.length + 1
            descr.append(cw, StyleSpan(Typeface.ITALIC), 0)
            descr.setSpan(UnderlineSpan(), offset, offset + cw.length, 0)
            descr.setSpan(
                    undecoratedClickable { bindDescription(account, "", caption) },
                    offset, offset + cw.length, 0
            )
        }

        return descr
    }

    private fun undecoratedClickable(f: () -> Unit): UpdateAppearance =
            object : ClickableSpan() {
                override fun onClick(widget: View) = f()
                override fun updateDrawState(ds: TextPaint) {}
            }
}
