package app.flashbulb.android.ui

import android.widget.ImageView

interface IconUpdater {
    fun update(view: ImageView)
}
