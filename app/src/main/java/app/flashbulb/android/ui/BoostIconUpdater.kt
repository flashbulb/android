package app.flashbulb.android.ui

import android.widget.ImageView
import app.flashbulb.android.R

abstract class BoostIconUpdater : IconUpdater {
    companion object {
        fun build(hasBoosted: Boolean, isBoostable: Boolean) =
                if (isBoostable) {
                    if (hasBoosted) {
                        BoostIconUpdater.Boosted()
                    } else {
                        BoostIconUpdater.NotBoosted()
                    }
                } else {
                    BoostIconUpdater.NotBoostable()
                }
    }

    override fun update(view: ImageView) {
        view.isClickable = isClickable
        view.isFocusable = isFocusable

        view.setImageResource(drawable)
        view.contentDescription =
                view.context.resources.getString(description)
    }

    protected abstract val drawable: Int
    protected abstract val description: Int
    protected abstract val isClickable: Boolean
    protected abstract val isFocusable: Boolean

    class Boosted : BoostIconUpdater() {
        override val drawable = R.drawable.arrow_decision
        override val description = R.string.boosted
        override val isClickable = true
        override val isFocusable = true
    }

    class NotBoosted : BoostIconUpdater() {
        override val drawable = R.drawable.sync
        override val description = R.string.not_boosted
        override val isClickable = true
        override val isFocusable = true
    }

    class NotBoostable : BoostIconUpdater() {
        override val drawable = R.drawable.sync_off
        override val description = R.string.not_boostable
        override val isClickable = false
        override val isFocusable = false
    }
}
