package app.flashbulb.android.auth

import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import android.view.View
import app.flashbulb.android.R
import app.flashbulb.android.api.Api
import app.flashbulb.android.databinding.AuthBinding
import app.flashbulb.android.support.FlashbulbActivity
import app.flashbulb.android.support.errorString
import app.flashbulb.android.support.hideKeyboard
import app.flashbulb.android.support.normalizedBaseUrl
import java.io.IOException
import kotlin.concurrent.thread


class AuthActivity : FlashbulbActivity() {
    companion object {
        private const val PREF_CLIENT_ID = "clientId"
        private const val PREF_CLIENT_SECRET = "clientSecret"
        private const val REDIRECT_URI = "flashbulb://flashbulb.app/oauth_redirect"
        const val ACCOUNT_NAME = "accountName"
    }

    private var accountAuthenticatorResponse: AccountAuthenticatorResponse? = null
    private var resultBundle: Bundle? = null
    private lateinit var binding: AuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = AuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.signIn.setOnClickListener(::addAccount)

        accountAuthenticatorResponse = intent.getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE)
        accountAuthenticatorResponse?.onRequestContinued()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        this.intent = intent
    }

    override fun onResume() {
        super.onResume()

        if (baseUrl() != null && resumingSignIn()) {
            binding.instanceHostname.setText(baseUrl()!!)
            resumeSignIn()
        }
    }

    private fun resumingSignIn() =
            intent.data != null &&
                    intent?.data?.pathSegments?.get(0) == "oauth_redirect" &&
                    intent?.data?.getQueryParameter("code") != null

    private fun resumeSignIn() {
        showThrobber()
        val api = Api.api(baseUrl()!!)

        thread(start = true) {
            try {
                val response =
                        api.requestAccessToken(
                                clientId = clientId()!!,
                                clientSecret = clientSecret()!!,
                                code = codeQueryParam(),
                                redirectUri = REDIRECT_URI
                        )
                                .execute()

                if (response.isSuccessful && response.body() != null) {
                    val accessToken = response.body()!!.accessToken
                    val authedApi = Api.api(baseUrl()!!, accessToken)
                    val credentialsResp = authedApi.verifyCredentials().execute()

                    if (credentialsResp.isSuccessful && credentialsResp.body() != null) {
                        val accountResp = credentialsResp.body()!!
                        val hostname = Uri.parse(baseUrl()!!).host
                        val accountName = "@${accountResp.username}@$hostname"
                        val account = Account(
                                accountName,
                                Authenticator.ACCOUNT_TYPE
                        )
                        val dbAccount = app.flashbulb.android.db.Account(
                                json = accountResp,
                                accountName = accountName,
                                baseUrl = baseUrl()!!
                        )

                        if (accountManager.addAccountExplicitly(account, null, null)) {
                            dao.insertAccount(dbAccount)
                            accountManager.setAuthToken(account, Authenticator.ACCOUNT_TYPE, accessToken)
                            oauthPrefs.edit().putString(ACCOUNT_NAME, account.name).apply()

                            val intent = Intent().apply {
                                putExtra(AccountManager.KEY_ACCOUNT_NAME, Authenticator.ACCOUNT_TYPE)
                                putExtra(AccountManager.KEY_ACCOUNT_TYPE, Authenticator.ACCOUNT_TYPE)
                                putExtra(AccountManager.KEY_AUTHTOKEN, accessToken)
                            }

                            setAccountAuthenticatorResult(intent.extras)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        } else {
                            runOnUiThread {
                                snackbar(R.string.account_manager_failed)
                                        .setAction(R.string.retry) { resumeSignIn() }
                                        .show()
                            }
                        }
                    } else {
                        runOnUiThread {
                            snackbar(getString(R.string.access_token_failed_reason, credentialsResp.errorString()))
                                    .setAction(R.string.retry) { resumeSignIn() }
                                    .show()
                        }
                    }
                } else {
                    runOnUiThread {
                        binding.instanceHostnameWrapper.error = getString(R.string.access_token_failed)
                        snackbar(R.string.access_token_failed)
                                .setAction(R.string.retry) { resumeSignIn() }
                                .show()
                    }
                }
            } catch (e: android.database.SQLException) {
                runOnUiThread {
                    snackbar(getString(R.string.database_error_reason, e.message)).show()
                }
            } catch (e: IOException) {
                runOnUiThread {
                    binding.instanceHostnameWrapper.error = getString(R.string.access_token_failed)
                    snackbar(getString(R.string.access_token_failed_reason, e.message))
                            .setAction(R.string.retry) { resumeSignIn() }
                            .show()
                }
            } finally {
                runOnUiThread {
                    hideThrobber()
                }
            }
        }
    }

    private fun codeQueryParam() = intent?.data?.getQueryParameter("code")!!
    private fun clientId() = oauthPrefs.getString("clientId", null)
    private fun clientSecret() = oauthPrefs.getString("clientSecret", null)

    private fun snackbar(@StringRes resId: Int, duration: Int = Snackbar.LENGTH_LONG) =
            Snackbar.make(binding.authLayout, resId, duration)

    private fun snackbar(msg: CharSequence, duration: Int = Snackbar.LENGTH_LONG) =
            Snackbar.make(binding.authLayout, msg, duration)

    private fun addAccount(view: View) {
        hideKeyboard(this, view)
        showThrobber()
        binding.instanceHostnameWrapper.error = null
        val instanceUrlFromUser: String = binding.instanceHostname.text.toString()

        thread(start = true) {
            normalizedBaseUrl(
                    instanceUrlFromUser,
                    { resId: Int, arg: String? ->
                        runOnUiThread {
                            binding.instanceHostnameWrapper.error = getString(resId, arg)
                            hideThrobber()
                        }
                    },
                    { baseUrl ->
                        runOnUiThread {
                            addAccountFor(baseUrl)
                        }
                    }
            )
        }
    }

    private fun addAccountFor(baseUrl: String) {
        val api = Api.api(baseUrl)
        val oauthPrefsEditor = oauthPrefs.edit()

        thread(start = true) {
            try {
                val response = api.registerApp().execute()

                if (response.isSuccessful && response.body() != null) {
                    val oauthApp = response.body()!!
                    val clientId = oauthApp.clientId

                    oauthPrefsEditor
                            .putString(PREF_CLIENT_ID, clientId)
                            .putString(PREF_CLIENT_SECRET, oauthApp.clientSecret)
                            .putString(PREF_BASE_URL, baseUrl)
                            .apply()

                    startActivity(
                            Intent(Intent.ACTION_VIEW).apply {
                                data = redirectUri(baseUrl, clientId)
                            }
                    )
                } else {
                    runOnUiThread {
                        binding.instanceHostnameWrapper.error = getString(R.string.app_registration_failed)
                        snackbar(R.string.app_registration_failed)
                                .setAction(R.string.retry, ::addAccount)
                                .show()
                    }
                }
            } catch (e: IOException) {
                runOnUiThread {
                    binding.instanceHostnameWrapper.error = getString(R.string.app_registration_failed)
                    snackbar(getString(R.string.app_registration_failed_reason, e.message))
                            .setAction(R.string.retry, ::addAccount)
                            .show()
                }
            } finally {
                runOnUiThread {
                    hideThrobber()
                }
            }
        }
    }

    private fun showThrobber() {
        binding.signIn.visibility = View.GONE
        binding.authProgress.visibility = View.VISIBLE
    }

    private fun hideThrobber() {
        binding.signIn.visibility = View.VISIBLE
        binding.authProgress.visibility = View.GONE
    }

    private fun redirectUri(baseUrl: String, clientId: String): Uri {
        val url = "$baseUrl/oauth/authorize?scope=read%20write%20follow&response_type=code&redirect_uri=$REDIRECT_URI&client_id=$clientId"

        return Uri.parse(url)
    }

    // From AccountAuthenticatorActivity

    private fun setAccountAuthenticatorResult(result: Bundle?) {
        resultBundle = result
    }

    override fun finish() {
        if (resultBundle != null) {
            accountAuthenticatorResponse?.onResult(resultBundle)
        } else {
            accountAuthenticatorResponse?.onError(AccountManager.ERROR_CODE_CANCELED,
                    "canceled")
        }
        accountAuthenticatorResponse = null

        super.finish()
    }

    private fun baseUrl(): String? =
            oauthPrefs.getString(PREF_BASE_URL, null)
}
