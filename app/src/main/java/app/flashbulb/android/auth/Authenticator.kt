package app.flashbulb.android.auth

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.os.Bundle

class Authenticator(private val context: Context) : AbstractAccountAuthenticator(context) {
    companion object {
        const val ACCOUNT_TYPE = "app.flashbulb.android.account"

    }

    override fun addAccount(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            accountType: String?,
            authTokenType: String?,
            requiredFeatures: Array<out String>?,
            options: Bundle?
    ): Bundle? {
        val intent = Intent(context, AuthActivity::class.java).apply {
            putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType)
            putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse)
        }

        return Bundle().apply {
            putParcelable(AccountManager.KEY_INTENT, intent)
        }
    }

    override fun confirmCredentials(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            account: Account?,
            bundle: Bundle?
    ): Bundle? {
        return null
    }

    override fun getAuthToken(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            account: Account,
            authTokenType: String?,
            options: Bundle?
    ): Bundle {
        val bundle = Bundle()
        val accountManager = AccountManager.get(context)

        if (authTokenType != ACCOUNT_TYPE) {
            bundle.apply {
                putString(AccountManager.KEY_ERROR_MESSAGE, "unknown auth token type")
            }
        } else if (accountManager.peekAuthToken(account, authTokenType) != null) {
            val accessToken = accountManager.peekAuthToken(account, authTokenType)
            bundle.apply {
                putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
                putString(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE)
                putString(AccountManager.KEY_AUTHTOKEN, accessToken)
            }
        } else {
            val intent = Intent(context, AuthActivity::class.java).apply {
                putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse)
            }
            bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        }

        return bundle
    }

    override fun getAuthTokenLabel(s: String?): String? {
        return if (s == ACCOUNT_TYPE) s else null // TODO something prettier perhaps
    }

    override fun hasFeatures(accountAuthenticatorResponse: AccountAuthenticatorResponse, account: Account?,
                             strings: Array<out String>?): Bundle {
        val bundle = Bundle()
        bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false)
        return bundle
    }

    override fun editProperties(accountAuthenticatorResponse: AccountAuthenticatorResponse, s: String?): Bundle {
        throw UnsupportedOperationException("this authenticator does not support editing properties")
    }

    override fun updateCredentials(accountAuthenticatorResponse: AccountAuthenticatorResponse, account: Account?, s: String?,
                                   bundle: Bundle?): Bundle {
        throw UnsupportedOperationException("this authenticator does not support updating credentials")
    }
}
