package app.flashbulb.android.auth

import android.accounts.AccountManager
import android.accounts.OnAccountsUpdateListener
import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import app.flashbulb.android.db.FlashbulbDatabase
import kotlin.concurrent.thread


class AuthenticatorService : Service() {
    private val authenticator: Authenticator by lazy {
        Authenticator(this)
    }

    private val accountManager: AccountManager by lazy {
        AccountManager.get(this)
    }

    private val listener: OnAccountsUpdateListener = OnAccountsUpdateListener { accounts ->
        thread(start = true) {
            val dao = FlashbulbDatabase.instance(applicationContext).dao()
            dao.keepOnlyAccountsWithAccountNames(accounts.map { it.name })
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return authenticator.iBinder
    }

    override fun onCreate() {
        super.onCreate()
        accountManager.addOnAccountsUpdatedListener(
                listener,
                Handler(),
                true
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        accountManager.removeOnAccountsUpdatedListener(listener)
    }
}
