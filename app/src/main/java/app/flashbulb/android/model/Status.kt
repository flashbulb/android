package app.flashbulb.android.model

import androidx.lifecycle.LiveData
import androidx.work.WorkInfo
import app.flashbulb.android.api.Account
import app.flashbulb.android.api.MediaAttachment
import app.flashbulb.android.api.Visibility
import app.flashbulb.android.ui.BoostIconUpdater
import app.flashbulb.android.ui.EntryViewHolder
import app.flashbulb.android.ui.FavoriteIconUpdater
import app.flashbulb.android.ui.IconUpdater

interface Status {
    val id: String
    val account: Account
    var hasFavorited: Boolean
    var hasBoosted: Boolean
    val isBoostable: Boolean

    fun isTheSameAs(status: Status): Boolean
    fun bind(viewHolder: EntryViewHolder)

    fun toggleFavorite(fav: (String) -> LiveData<WorkInfo>, unfav: (String) -> LiveData<WorkInfo>): LiveData<WorkInfo>
    fun toggleBoost(boost: (String) -> LiveData<WorkInfo>, unboost: (String) -> LiveData<WorkInfo>): LiveData<WorkInfo>

    fun boostIconUpdater(): IconUpdater
    fun favoriteIconUpdater(): IconUpdater
}

data class BoostedStatus(
        override val id: String,
        val uri: String,
        val url: String?,
        override val account: Account, // TODO
        val inReplyToId: Long?,
        val inReplyToAccountId: Long?,
        val reblog: Status,
        val content: String,
        val createdAt: String,
        val reblogsCount: Long,
        val favoritesCount: Long,
        override var hasBoosted: Boolean,
        override var hasFavorited: Boolean,
        val hasMuted: Boolean,
        val spoilerText: String,
        val visibility: Visibility, // TODO
        val mediaAttachments: List<MediaAttachment>, // TODO
        val language: String?,
        val pinned: Boolean
) : Status {
    override val isBoostable = visibility.isBoostable

    override fun isTheSameAs(status: Status): Boolean =
            status is BoostedStatus && id == status.id

    override fun bind(viewHolder: EntryViewHolder) {
        reblog.bind(viewHolder)
        viewHolder.bindBoosterAccountName(account)
    }

    override fun toggleFavorite(fav: (String) -> LiveData<WorkInfo>, unfav: (String) -> LiveData<WorkInfo>) =
            reblog.toggleFavorite(fav, unfav)

    override fun toggleBoost(boost: (String) -> LiveData<WorkInfo>, unboost: (String) -> LiveData<WorkInfo>) =
            reblog.toggleBoost(boost, unboost)

    override fun boostIconUpdater() = reblog.boostIconUpdater()
    override fun favoriteIconUpdater() = reblog.favoriteIconUpdater()

}

data class SimpleStatus(
        override val id: String,
        val uri: String,
        val url: String?,
        override val account: Account, // TODO
        val inReplyToId: Long?,
        val inReplyToAccountId: Long?,
        val content: String,
        val createdAt: String,
        val reblogsCount: Long,
        val favoritesCount: Long,
        override var hasBoosted: Boolean,
        override var hasFavorited: Boolean,
        val hasMuted: Boolean,
        val spoilerText: String,
        val visibility: Visibility, // TODO
        val mediaAttachments: List<MediaAttachment>, // TODO
        val language: String?,
        val pinned: Boolean
) : Status {
    override val isBoostable = visibility.isBoostable

    override fun isTheSameAs(status: Status): Boolean =
            status is SimpleStatus && id == status.id

    override fun bind(viewHolder: EntryViewHolder) {
        viewHolder.bindActions(this)
        viewHolder.bindMedia(mediaAttachments)
        viewHolder.bindAccountAvatar(account)
        viewHolder.bindDescription(account, spoilerText, content)
        viewHolder.bindVisibility(visibility.iconUpdater)
        viewHolder.bindFavoriteStatus(favoriteIconUpdater())
        viewHolder.bindBoostStatus(boostIconUpdater())
        viewHolder.clearBoosterAccountName()
    }

    override fun toggleFavorite(fav: (String) -> LiveData<WorkInfo>, unfav: (String) -> LiveData<WorkInfo>) =
            if (hasFavorited)
                unfav(id)
            else
                fav(id)

    override fun toggleBoost(boost: (String) -> LiveData<WorkInfo>, unboost: (String) -> LiveData<WorkInfo>) =
            if (hasBoosted)
                unboost(id)
            else
                boost(id)

    override fun boostIconUpdater() = BoostIconUpdater.build(hasBoosted, isBoostable)
    override fun favoriteIconUpdater() = FavoriteIconUpdater.build(hasFavorited)
}

data class SensitiveStatus(
        override val id: String,
        val uri: String,
        val url: String?,
        override val account: Account, // TODO
        val inReplyToId: Long?,
        val inReplyToAccountId: Long?,
        val content: String,
        val createdAt: String,
        val reblogsCount: Long,
        val favoritesCount: Long,
        override var hasBoosted: Boolean,
        override var hasFavorited: Boolean,
        val hasMuted: Boolean,
        val spoilerText: String,
        val visibility: Visibility, // TODO
        val mediaAttachments: List<MediaAttachment>, // TODO
        val language: String?,
        val pinned: Boolean
) : Status {
    override val isBoostable = visibility.isBoostable

    override fun isTheSameAs(status: Status): Boolean =
            status is SensitiveStatus && id == status.id

    override fun bind(viewHolder: EntryViewHolder) {
        viewHolder.bindActions(this)
        viewHolder.bindSensitiveMedia(mediaAttachments)
        viewHolder.bindAccountAvatar(account)
        viewHolder.bindDescription(account, spoilerText, content)
        viewHolder.bindVisibility(visibility.iconUpdater)
        viewHolder.bindFavoriteStatus(favoriteIconUpdater())
        viewHolder.bindBoostStatus(boostIconUpdater())
        viewHolder.clearBoosterAccountName()
    }

    override fun toggleFavorite(fav: (String) -> LiveData<WorkInfo>, unfav: (String) -> LiveData<WorkInfo>) =
            if (hasFavorited)
                unfav(id)
            else
                fav(id)

    override fun toggleBoost(boost: (String) -> LiveData<WorkInfo>, unboost: (String) -> LiveData<WorkInfo>) =
            if (hasBoosted)
                unboost(id)
            else
                boost(id)

    override fun boostIconUpdater() = BoostIconUpdater.build(hasBoosted, isBoostable)
    override fun favoriteIconUpdater() = FavoriteIconUpdater.build(hasFavorited)
}
